const express = require('express');
const piezasMateriales = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

piezasMateriales.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM piezasMateriales', (err, rows) => {
            if(err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

piezasMateriales.post('/', (req, res) => {
    req.getConnection((err, connection) => {

        connection.query('INSERT INTO piezasMateriales SET ?', [req.body], (err, rows) => {
            if(err) return res.send(err)

            res.send('Se agrego exitosamente')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

piezasMateriales.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM piezasMateriales WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Actualizar)

piezasMateriales.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE piezasMateriales SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se actualizo exitosamente')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = piezasMateriales