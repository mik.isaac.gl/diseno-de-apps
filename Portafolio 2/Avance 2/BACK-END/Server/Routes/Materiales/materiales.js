const express = require('express');
const materiales = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

materiales.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM materiales', (err, rows) => {
            if (err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

materiales.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('INSERT INTO materiales SET ?', [req.body], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se agrego exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

materiales.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM materiales WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Actualizar)

materiales.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE materiales SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se actualizo exitosamente')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = materiales