const express = require('express');
const reportes = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

reportes.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM reportes', (err, rows) => {
            if (err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

reportes.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('INSERT INTO reportes SET ?', [req.body], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se agrego exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

reportes.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM reportes WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});


//--------------------------------------------------Ruta de prueba(Actualizar)

reportes.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE reportes SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se actualizo exitosamente')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = reportes