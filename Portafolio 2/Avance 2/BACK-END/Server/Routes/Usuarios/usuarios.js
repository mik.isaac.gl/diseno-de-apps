const express = require('express');
const usuarios = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

usuarios.get('/', (req,res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM Usuarios', (err, rows) => {
            if (err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

usuarios.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('INSERT INTO Usuarios SET ?', [req.body], (err, rows) => {
            if(err) return res.send(err)

            res.send('Se agrego exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

usuarios.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM Usuarios WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Actualizar)

usuarios.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE Usuarios SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se actualizo correctamente')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = usuarios
