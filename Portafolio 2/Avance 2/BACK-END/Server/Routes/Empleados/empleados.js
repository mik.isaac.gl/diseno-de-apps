const express = require('express');
const empleados = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

empleados.get('/', (req, res)=> {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM Empleados', (err, rows) => {
            if(err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

empleados.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('INSERT INTO Empleados SET ?', [req.body], (err, rows) => {
            if(err) return res.send(err)
            
            res.send('Se agrego exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

empleados.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM Empleados WHERE numero = ?', [req.params.numero], (err, rows) => {
            if(err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Actualizar)

empleados.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE Empleados SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se actualizo exitosamente!')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = empleados