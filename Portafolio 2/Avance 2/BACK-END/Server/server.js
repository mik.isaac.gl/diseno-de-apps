const express = require('express')
const mysql = require('mysql')
const connection = require('express-myconnection')
const jwt = require('jsonwebtoken');
//-----------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------RUTAS
//-----------------------------------------------------------------------------------------------------
const apDispositivos = require('./Routes/Ap_Dispositivos/apDispositivos')//-------------Ap_Dispositivos
const apModelos = require('./Routes/Ap_Modelos/apModelos')//----------------------------Ap_Modelos
const areasProduccion = require('./Routes/Areas_Produccion/areasProduccion')//----------Areas_Produccion
const dispositivos = require('./Routes/Dispositivos/dispositivos')//--------------------Dispositivos
const empleados = require('./Routes/Empleados/empleados')//-----------------------------Empleados
const estados = require('./Routes/Estados/estados')//-----------------------------------Estados
const materiales = require('./Routes/Materiales/materiales')//--------------------------Materiales
const modeloPiezas = require('./Routes/Modelo_Piezas/modeloPiezas')//-------------------ModeloPiezas
const modelos = require('./Routes/Modelos/modelos')//-----------------------------------Modelos
const piezas = require('./Routes/Piezas/piezas')//--------------------------------------Piezas
const piezasMateriales = require('./Routes/Piezas_Materiales/piezasMateriales')//-------PiezasMateriales
const puestos = require('./Routes/Puestos/puestos')//-----------------------------------Puestos
const reportes = require('./Routes/Reportes/reportes')//--------------------------------Reportes
const tareas = require('./Routes/Tareas/tareas')//--------------------------------------Tareas
const usuarios = require('./Routes/Usuarios/usuarios')//--------------------------------Usuarios
//-----------------------------------------------------------------------------------------------------

const app = express()
app.set('port', process.env.PORT || 8000)

//----------------------------Base de datos
const db = {
    host:'localhost',
    port: 3306,
    user:'root',
    password:'',
    database:'produccion'

}
//----------------------------Conexión a la base de datos
app.use(connection(mysql, db, 'single'))
app.use(express.json())
app.use(express.static(__dirname));
//----------------------------Ruta principal
app.get('/', (req, res) => {
    res.redirect('/Html/welcome.html')
});

app.post('/login', (req, res) => {
    const { correo, contrasena } = req.body;

    req.getConnection((err, connection) => {
        if (err) {
            return res.status(500).json({ error: 'Error de conexión a la base de datos' });
        }

//----------------------------Buscar el usuario en la base de datos por el correo electrónico
        connection.query('SELECT numero, correo, contrasena FROM Usuarios WHERE correo = ?', [correo], (err, rows) => {
            if (err) {
                return res.status(500).json({ error: 'Error al realizar la consulta SQL' });
            }

            if (rows.length === 0) {
                return res.status(401).json({ error: 'Correo no encontrado' });
            }

            const user = rows[0];

//----------------------------Verificar contraseña
            if (contrasena !== user.contrasena) {
                return res.status(401).json({ error: 'Contraseña incorrecta' });
            }

//----------------------------Generar un token JWT
            jwt.sign({ user }, 'secretkey', { expiresIn: '60s' }, (jwtErr, token) => {
                if (jwtErr) {
                    return res.status(500).json({ error: 'Error al generar el token JWT' });
                }

                res.json({ token });
            });
        });
    });
});

function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined') { 
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }
}

//----------------------------ruta de apDispositivos
app.use('/apDispositivos', verifyToken, apDispositivos);

//----------------------------ruta de apModelos
app.use('/apModelos', verifyToken, apModelos);

//----------------------------ruta de areas de produccion
app.use('/areasProduccion', verifyToken, areasProduccion);

//----------------------------ruta de dispositivos
app.use('/dispositivos', verifyToken, dispositivos);

//----------------------------ruta de empleados
app.use('/empleados', empleados);

//----------------------------ruta de estados
app.use('/estados', verifyToken, estados);

//----------------------------ruta de materiales
app.use('/materiales', verifyToken, materiales);

//----------------------------ruta de modeloPiezas
app.use('/modeloPiezas', verifyToken, modeloPiezas);

//----------------------------ruta de Modelos
app.use('/modelos', verifyToken, modelos);

//----------------------------ruta de piezas
app.use('/piezas', verifyToken, piezas);

//----------------------------ruta de piezasMateriales
app.use('/piezasMateriales', verifyToken, piezasMateriales);

//----------------------------ruta de puestos
app.use('/puestos', verifyToken, puestos);

//----------------------------ruta de reportes
app.use('/reportes', verifyToken, reportes);

//----------------------------ruta de tareas
app.use('/tareas', verifyToken, tareas);

//----------------------------ruta de usuarios
app.use('/usuarios', usuarios);

//Servidor
app.listen(app.get('port'), ()=>{
    console.log('Server running on port', app.get('port'))
})
