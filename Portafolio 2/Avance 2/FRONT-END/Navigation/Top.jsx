import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { Tools } from "../Screens/Main";

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Tab = createMaterialBottomTabNavigator();
function Top() {
  return (
    <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Tools"
          activeColor="#4B7583"
          inactiveColor="#f0edf6"
          barStyle={{ backgroundColor: '#6D4B70' }}
        >
          <Tab.Screen
            name="Tools"
            component={Tools}
            options={{
              tabBarLabel: 'Herramientas',
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="tools" color={color} size={26} />
              ),
            }}
          />
        </Tab.Navigator>
    </NavigationContainer>
  );
}

export default Top;


{/* <Tab.Screen
name="Almacen"
component={Almacen}
options={{
  tabBarLabel: 'Almacen',
  tabBarIcon: ({ color }) => (
    <MaterialCommunityIcons name="archive" color={color} size={26} />
  ),
}}
/>
<Tab.Screen
name="Empleados"
component={Empleados}
options={{
  tabBarLabel: 'Empleados',
  tabBarIcon: ({ color }) => (
    <MaterialCommunityIcons name="account" color={color} size={26} />
  ),
  
}}
/>
<Tab.Screen
name="Login"
component={Login}
options={{
  tabBarLabel: 'login',
  tabBarIcon: ({ color }) => (
    <MaterialCommunityIcons name="login" color={color} size={26} />
  ),
}}
/> */}