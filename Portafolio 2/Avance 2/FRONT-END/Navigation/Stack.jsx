import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Almacen, Empleados, Login, Tools } from "../Screens/Main"
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Stack = createNativeStackNavigator();
const Tab = createMaterialBottomTabNavigator();
function StackNav() {
  return (
    <NavigationContainer >
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }}/>
            <Stack.Screen name="Almacen" component={Almacen} options={{ headerShown: false }}/>
            <Stack.Screen name="Empleados" component={Empleados} options={{ headerShown: false }}/>
            <Stack.Screen name="Main" component={Top} options={{ headerShown: false }} />
        </Stack.Navigator>
    </NavigationContainer>
  );
}

function Top() {
    return (
      
          <Tab.Navigator
            initialRouteName="Tools"
            activeColor="#4B7583"
            inactiveColor="#f0edf6"
            barStyle={{ backgroundColor: '#6D4B70' }}
          >
            <Tab.Screen
              name="Tools"
              component={Tools}
              options={{
                tabBarLabel: 'Herramientas',
                tabBarIcon: ({ color }) => (
                  <MaterialCommunityIcons name="tools" color={color} size={26} />
                ),
              }}
            />
          </Tab.Navigator>
      
    );
  }

export default StackNav;