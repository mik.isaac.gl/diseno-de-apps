import { VStack, View, } from '@gluestack-ui/themed';
import React from 'react';
import {  StyleSheet } from "react-native";
import { Title, HeadChart, EmployeeList } from "../Components/Main";

 const Empleados = () => {
  return (
    <View style={styles.container}>
      <VStack alignItems="stretch" alignContent="space-between">
        <HeadChart />
        <View>
          <VStack>
            <View>
              <Title text="Empleados"/>
            </View>
            <View alignItems="center">
                <EmployeeList />
            </View>
          </VStack>
        </View>
      </VStack>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-between',
      backgroundColor: '#EAEBF0'
    },
  });

  export default React.memo(Empleados);
