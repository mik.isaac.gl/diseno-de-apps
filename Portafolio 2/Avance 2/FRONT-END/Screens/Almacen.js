import { GluestackUIProvider, VStack, View, ScrollView } from '@gluestack-ui/themed';
import React from 'react';
import { StyleSheet } from 'react-native';
import { config } from '@gluestack-ui/config';
import { HeadChart, GraphicCard, Title } from '../Components/Main';

const Almacen = () => {
  return (
    <View style={styles.container}>
      <GluestackUIProvider config={config}>
        <ScrollView>
          <VStack alignItems="stretch" alignContent="space-between">
            {/* Head */}
            <HeadChart />
            {/* EndHead */}
            <View>
              <VStack>
                {/* Title of Content */}
                <Title text="Almacen" />
                {/* EndTitle */}
                <View alignItems="center" bg="primary.400" rounded="lg">
                  <VStack space="2xl">
                    {/* Content */}
                    <GraphicCard text="Piezas" color="#6D4B70" />
                    <GraphicCard text="Modelos" color="#47516C" />
                    <GraphicCard text="Materiales" color="#6D4B70" />
                    {/* EndContent */}
                  </VStack>
                </View>
              </VStack>
            </View>
          </VStack>
        </ScrollView>
      </GluestackUIProvider>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'space-between',
    backgroundColor: '#EAEBF0',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333',
  },
});

export default Almacen;
