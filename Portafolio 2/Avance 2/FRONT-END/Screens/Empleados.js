import { GluestackUIProvider, VStack, View, } from '@gluestack-ui/themed';
import React from 'react';
import {  StyleSheet } from "react-native";
import { Title, HeadChart, EmployeeList } from "../Components/Main";
import { config } from '@gluestack-ui/config';
  

 const Empleados = () => {
  return (
    <View style={styles.container}>
      <GluestackUIProvider config={config}>
        {/* <ScrollView > */}
          <VStack alignItems="stretch" alignContent="space-between">
            {/* //////////////////////////////////////////////////////////////// */}
            {/* Head */}


            <HeadChart />


            {/* EndHead */}
            {/* //////////////////////////////////////////////////////////////// */}

            {/* //////////////////////////////////////////////////////////////// */}
            {/* //////////////////////////////////////////////////////////////// */}
            <View>
              <VStack>
                <View>
                {/* //////////////////////////////////////////////////////////////// */}
                {/* Title of Content */}


                  <Title text="Empleados"/>

                  
                {/* Title of Content */}
                {/* //////////////////////////////////////////////////////////////// */}
                </View>
                {/* EndTitle */}
                {/* //////////////////////////////////////////////////////////////// */}
                <View alignItems="center">
                  <VStack>
                    {/* //////////////////////////////////////////////////////////////// */}
                    {/* Content */}


                      <EmployeeList />


                    {/* EndContent */}
                    {/* //////////////////////////////////////////////////////////////// */}
                  </VStack>
                </View>
              </VStack>
            </View>
            {/* //////////////////////////////////////////////////////////////// */}
            {/* //////////////////////////////////////////////////////////////// */}
          </VStack>
        {/* </ScrollView> */}
      </GluestackUIProvider>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-between',
      backgroundColor: '#EAEBF0'
    },
  });

  export default Empleados;
