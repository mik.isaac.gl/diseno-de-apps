import { Center, Text, Box, HStack, Avatar, AvatarImage, VStack, FlatList  } from '@gluestack-ui/themed';
import React from 'react';
import { View, StyleSheet } from 'react-native';

// Configuración del gráfico


export const EmployeeList = () => {

  const  Empleado = ({ fullName, recentText, avatarUrl }) => {
    return (
      <Box
        borderBottomWidth="$1"
        borderColor="$trueGray800"
        sx={{
          _dark: {
            borderColor: "$trueGray100",
          },
          "@base": {
            pl: 0,
            pr: 0,
          },
          "@sm": {
            pl: "$4",
            pr: "$5",
          },
        }}
        py="$2"
      >
        <HStack space="md" justifyContent="space-start">
          <Avatar alignSelf='center' size="md">
            <AvatarImage alt='si' source={{ uri: avatarUrl }} />
          </Avatar>
          <VStack>
            <Text
              
            >
              {fullName}
            </Text>
            <Text
             
            >
              {recentText}
            </Text>
          </VStack>
        </HStack>
      </Box>
    );
  }

  const data = [
    {
      id: '1',
      fullName: 'Martinez Zavala Jonathan',
      recentText: 'Scrum Master',
      avatarUrl:'https://scontent-lax3-1.xx.fbcdn.net/v/t39.30808-1/288716184_1071113500429810_8687112587320319758_n.jpg?stp=dst-jpg_p200x200&_nc_cat=108&ccb=1-7&_nc_sid=5740b7&_nc_eui2=AeGYitKuRy96igJUBLL281Am8u1GIfWZoKry7UYh9ZmgqqiKvZgxymAFDFCLOwnPyggniVRkATkI_q1OKBLRibxW&_nc_ohc=a9uRGijTYGsAX-kvCcP&_nc_oc=AQnF0OpM41fq1eBQ77hWKOvWuxhY3H3TMOjvb7rBHgVHD8FyyTx6mkLTBQjC6QxCABQ&_nc_ht=scontent-lax3-1.xx&oh=00_AfB1PfG3DL_vN5n8bBxxKONhsHS6Y1NMdIKOV5wwx4m9WA&oe=65E2AF6B',
    },
    {
      id: '2',
      fullName: 'Muñoz Carpinteyro Angel ',
      recentText: 'DataBase',
      avatarUrl:'https://scontent-lax3-2.xx.fbcdn.net/v/t39.30808-6/352225709_156532150644172_1241604560264064345_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=efb6e6&_nc_eui2=AeFWIN3VuytK5Qxxan5Qf3Gg4RWUz2G6VGzhFZTPYbpUbA5J_5Jz4ylpsob0_J6NXLBF0jBVmM0JB3KYxcTV6xE-&_nc_ohc=qI4B9MLh3kgAX8cD_ZI&_nc_ht=scontent-lax3-2.xx&oh=00_AfD_H2-hZA6EeQMUleAeo3Ea-W_pWyC1JpQpwscliccT6A&oe=65E4262B',
    },
    {
      id: '3',
      fullName: 'Quintana Zepeda Andrea ',   
      recentText: 'CEO',
      avatarUrl: 'https://calificaciones.uttijuana.edu.mx/sca/storage/students/0322103793.jpeg?format=1660148520349',
    },
    {
      id: '4',
      fullName: 'Garcia Lopez Isaac ',
      recentText: 'Scrum Master',
      avatarUrl:'https://scontent-lax3-1.xx.fbcdn.net/v/t39.30808-1/322201018_722995356007118_3218548556798039682_n.jpg?stp=dst-jpg_p200x200&_nc_cat=108&ccb=1-7&_nc_sid=5740b7&_nc_eui2=AeEoH4uc38I1CfzL9aGZbSVGzDfg1KoTUzzMN-DUqhNTPI78TAaAzGKtoF2yUeMHUDlQRJ9UxiFwvaTqQV2Z9Ks4&_nc_ohc=T8hLT67ipn4AX8f6hvW&_nc_ht=scontent-lax3-1.xx&oh=00_AfCEXBY1PDNuRjqeVXI-yXvog8-YWK5NtmGYxNY9kDZabA&oe=65E29E81',
    },
    {
      id: '5',
      fullName: 'Chavez Castillon Angel',   
      recentText: 'Junior',
      avatarUrl:'https://scontent-lax3-2.xx.fbcdn.net/v/t39.30808-6/405206792_122122573124075263_1836489551167178039_n.jpg?stp=cp6_dst-jpg&_nc_cat=101&ccb=1-7&_nc_sid=dd5e9f&_nc_eui2=AeGqU-PckXyUC1o2jlN13SvJ3hwyGKFVS6DeHDIYoVVLoAlJircgYSxk737qXOmvDWosOK4sYyQYSOuqL5__AoK7&_nc_ohc=JqQScTaDsI4AX9KyrHI&_nc_ht=scontent-lax3-2.xx&oh=00_AfB5ItUCWAj8zgrITGGxbXbPvQ8bb0NV6faCuQozMOHuMg&oe=65E2BE40',
    },
    {
      id: '6',
      fullName: 'Martinez Zavala Jonathan',
      recentText: 'Scrum Master',
      avatarUrl:'https://scontent-lax3-1.xx.fbcdn.net/v/t39.30808-1/288716184_1071113500429810_8687112587320319758_n.jpg?stp=dst-jpg_p200x200&_nc_cat=108&ccb=1-7&_nc_sid=5740b7&_nc_eui2=AeGYitKuRy96igJUBLL281Am8u1GIfWZoKry7UYh9ZmgqqiKvZgxymAFDFCLOwnPyggniVRkATkI_q1OKBLRibxW&_nc_ohc=a9uRGijTYGsAX-kvCcP&_nc_oc=AQnF0OpM41fq1eBQ77hWKOvWuxhY3H3TMOjvb7rBHgVHD8FyyTx6mkLTBQjC6QxCABQ&_nc_ht=scontent-lax3-1.xx&oh=00_AfB1PfG3DL_vN5n8bBxxKONhsHS6Y1NMdIKOV5wwx4m9WA&oe=65E2AF6B',
    },
    {
      id: '7',
      fullName: 'Muñoz Carpinteyro Angel ',
      recentText: 'DataBase',
      avatarUrl:'https://calificaciones.uttijuana.edu.mx/sca/storage/students/0322104102.jpeg',
    },
    {
      id: '8',
      fullName: 'Quintana Zepeda Andrea ',   
      recentText: 'CEO',
      avatarUrl: 'https://calificaciones.uttijuana.edu.mx/sca/storage/students/0322103793.jpeg?format=1660148520349',
    },
    {
      id: '9',
      fullName: 'Garcia Lopez Isaac ',
      recentText: 'Scrum Master',
      avatarUrl:'https://calificaciones.uttijuana.edu.mx/sca/storage/students/0322103717.jpeg',
    },
    {
      id: '10',
      fullName: 'Chavez Castillon Angel',   
      recentText: 'Junior',
      avatarUrl:'https://scontent-lax3-1.xx.fbcdn.net/v/t39.30808-6/393816268_122108429720075263_5763995963550044511_n.jpg?stp=cp6_dst-jpg_s206x206&_nc_cat=102&ccb=1-7&_nc_sid=3d9721&_nc_eui2=AeHsuviTHSFwpVmkBYD_V7BgKUKs-4bPqZIpQqz7hs-pkv10iEx6XEiHRJYdzcCocsPeVVEdiAsWi-EaCADczp9c&_nc_ohc=GStUKOS_wawAX-tFFCw&_nc_ht=scontent-lax3-1.xx&oh=00_AfDMQ9bhOtZz24WpCV7JIHbTXbZwWPS6fk4TkoZsoUay5w&oe=65E3E144',
    },
    


  ];

  return (
    <View>
      <Center w={360} h={440} px={10} py={10} bg="#FFFFFF"  borderRadius={25}>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <Empleado
              fullName={item.fullName}
              recentText={item.recentText}
              avatarUrl={item.avatarUrl}
            />
          )}
          keyExtractor={item => item.id}
        />
      </Center>
    </View>
  );
};

// Estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF', // Color de fondo del contenedor
  },

});



//scrollview
//flatLis