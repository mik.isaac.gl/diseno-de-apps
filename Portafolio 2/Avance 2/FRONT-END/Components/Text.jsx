import { View, Text } from '@gluestack-ui/themed';

export function Text(Text) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>{Text}</Text>
        </View>
    );
}