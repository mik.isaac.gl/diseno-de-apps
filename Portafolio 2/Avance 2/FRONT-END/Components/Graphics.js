import { Center } from '@gluestack-ui/themed';
import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { BarChart } from 'react-native-chart-kit';

const screenWidth = Dimensions.get('window').width;

// Configuración del gráfico
const chartConfig = {
  backgroundColor: 'transparent', // Fondo transparente
  backgroundGradientFromOpacity: 0,
  backgroundGradientToOpacity: 0,
  fillShadowGradientFrom: `#FFFFFF`,
  fillShadowGradientFromOpacity: 1,
  fillShadowGradientTo: `#FFFFFF`,
  fillShadowGradientToOpacity: 1,
  decimalPlaces: 0,
  color: () => '#FFFFFF', // Color de las barras
  labelColor: () => '#FFFFFF', // Color de las etiquetas
  style: {
    borderRadius: 16, // Esquinas redondeadas de las barras
  },
  propsForBackgroundLines: {
    strokeWidth: 0.5, // Grosor de las líneas de fondo
    stroke: 'rgba(200, 200, 200, 0.5)', // Color gris claro
    strokeDasharray: [0],
    strokeOpacity: 0.7,
    
  },
};

export const HeadChart = () => {
  // Datos de ejemplo para la gráfica
  const chartData = {
    labels: ["",'Elemento 1',"", "",'Elemento 2',"","",'Elemento 3',"",],
    datasets: [
      {
        data: [3, 6, 8, 14, 16, 18],
      },
    ],
  };

  return (
    <View>
      <Center bg="#2D3154" $ios-pt={40} $ios-h={220} height={170} width="$full">
        <View style={styles.container}>
          <View style={styles.chartContainer}>
            <BarChart
              data={chartData}
              width={screenWidth}
              height={160}
              yAxisLabel={'$'}
              fromZero
              bezier
              chartConfig={chartConfig}
              style={{
                marginVertical: 0,
                borderRadius: 24,
              }}
            />  
          </View>
        </View>
      </Center>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2D3154', // Color de fondo del contenedor
  },
  chartContainer: {
    paddingRight:30,
    height: 160, // Altura del gráfico
    width: '90%', // Ancho del gráfico
  },
});

