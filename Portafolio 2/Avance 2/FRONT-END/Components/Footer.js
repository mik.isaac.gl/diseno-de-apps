
import { View, Box } from "@gluestack-ui/themed";

export const Footer = ()  => {
    return (
        <View
        alignItems="flex-end"
    >
        <Box
            bg="#2D3154"
            height={60}
            width="$full"
            maxWidth="100%"
        />
    </View>
    );
}