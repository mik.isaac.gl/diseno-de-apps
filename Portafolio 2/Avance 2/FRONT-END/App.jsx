import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StatusBar } from 'react-native';
import { config } from "@gluestack-ui/config";
import { GluestackUIProvider } from '@gluestack-ui/themed';
import StackNav from "./Navigation/Stack";

const App = () => {
  return (
    <SafeAreaProvider>
      <StatusBar backgroundColor="#2D3154" hidden={false} />
      <GluestackUIProvider config={config}>
        <StackNav />
      </GluestackUIProvider>
    </SafeAreaProvider>
  );
}

export default App;


