
// Garcia Lopez Miguel Isaac 4D
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';

const App = () => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [filterType, setFilterType] = useState('');

  useEffect(() => {
    fetchData('http://jsonplaceholder.typicode.com/todos');
  }, []);

  const fetchData = async (url) => {
    try {
      const response = await fetch(url);
      const data = await response.json();
      setData(data);
    } catch (error) {
      console.error('Error fetching data: ', error);
    }
  };

  const filterData = (type) => {
    setFilterType(type);
    switch (type) {
      case 'allIds':
        setFilteredData(data.map(item => ({ id: item.id })));
        break;
      case 'IdsAndTitles':
        setFilteredData(data.map(item => ({ id: item.id, title: item.title })));
        break;
      case 'uncompletedTitle':
        setFilteredData(data.filter(item => !item.completed).map(item => ({ id: item.id, title: item.title })));
        break;
      case 'completedTitle':
        setFilteredData(data.filter(item => item.completed).map(item => ({ id: item.id, title: item.title })));
        break;
      case 'idsAndUserId':
        setFilteredData(data.map(item => ({ id: item.id, userId: item.userId })));
        break;
      case 'completedAndUserTitle':
        setFilteredData(data.filter(item => item.completed).map(item => ({ title: item.title, userId: item.userId })));
        break;
      case 'uncompletedAndUserTitle':
        setFilteredData(data.filter(item => !item.completed).map(item => ({ title: item.title, userId: item.userId })));
        break;
      default:
        setFilteredData([]);
        break;
    }
  };

  const clearFilter = () => {
    setFilterType('');
    setFilteredData([]);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Lista de Pendientes</Text>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity onPress={() => filterData('allIds')} style={styles.button}>
          <Text style={styles.buttonText}>Todos los IDs</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => filterData('IdsAndTitles')} style={styles.button}>
          <Text style={styles.buttonText}>IDs y Títulos</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => filterData('uncompletedTitle')} style={styles.button}>
          <Text style={styles.buttonText}>Sin Completar</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => filterData('completedTitle')} style={styles.button}>
          <Text style={styles.buttonText}>Completados</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => filterData('idsAndUserId')} style={styles.button}>
          <Text style={styles.buttonText}>IDs y UserID</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => filterData('completedAndUserTitle')} style={styles.button}>
          <Text style={styles.buttonText}>Completados y UserID</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => filterData('uncompletedAndUserTitle')} style={styles.button}>
          <Text style={styles.buttonText}>Sin Completar y UserID</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={clearFilter} style={styles.clearButton}>
          <Text style={styles.clearButtonText}>Limpiar</Text>
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.scrollView}>
        {filteredData.map((item, index) => (
          <View key={index} style={styles.dataItem}>
            {filterType.includes('Id') && <Text>{`ID: ${item.id}`}</Text>}
            {filterType.includes('Title') && <Text>{`Título: ${item.title}`}</Text>}
            {filterType.includes('User') && <Text>{`UserID: ${item.userId}`}</Text>}
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  buttonsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#007bff',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
    marginRight: 10,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  clearButton: {
    backgroundColor: '#dc3545',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  clearButtonText: {
    color: '#fff',
    textAlign: 'center',
  },
  scrollView: {
    flex: 1,
  },
  dataItem: {
    borderWidth: 1,
    borderColor: '#ddd',
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
  },
});

export default App;
