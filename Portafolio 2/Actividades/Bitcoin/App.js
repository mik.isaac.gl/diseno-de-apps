import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import BtcUsd from './components/BtcUsd';
import BtcEuro from './components/BtcEuro';
import BtcGbp from './components/BtcGbp';
import Post from './components/Post';
import Products from './components/Products';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <BtcUsd />
      </View>
      <View style={styles.box}>
        <BtcEuro />
      </View>
      <View style={styles.box}>
        <BtcGbp />
      </View>
      <View style={styles.box}>
        <Post />
      </View>
      <View style={styles.box}>
        <Products />
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'Green',
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
