// StackScreen.js
import React from "react";
import { View, Text, StyleSheet } from 'react-native';

export default function StackScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Stack Screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF'
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#007AFF'
  }
});
