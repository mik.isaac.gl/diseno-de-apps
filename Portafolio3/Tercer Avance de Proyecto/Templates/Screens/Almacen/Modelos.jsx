import { Body, SubTitle, CarouselCard, TitleBackCard, InfoCard, InfoCardModelos } from '../../Components/Mix/Main'
import { ScrollView, Image } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import { BigAvatar, Card, Button, Center, Title, H2, H1, Wrap } from '../../Components/Root/Main';
import * as Style from '../../Styles/Style';
import { modelos } from "../../../Core/Connection/rutas/Almacen/Modelos";
import { GET } from '../../../Core/Connection/Global';




const ModelosScreen = () => {       //AQUI
    const Navigate = Screens();
    const Modelos = GET('modelos');

    return (
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Almacen</TitleBackCard>
            <ScrollView>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 150}]}>
                <Title style={{marginBottom:10}}>Modelos</Title>
                <Title style={{marginBottom:10,alignSelf: "flex-start"}} >Aqui puedes encontrar todos los medicamentos que hay en almacen.</Title>
                        
            </Card>
            <InfoCardModelos  style={{width: 150}} ImgStyle={{flex: 0, height: "50%"}} list={Modelos}/>
            </ScrollView>
        </Body>
    )
}

export default ModelosScreen;