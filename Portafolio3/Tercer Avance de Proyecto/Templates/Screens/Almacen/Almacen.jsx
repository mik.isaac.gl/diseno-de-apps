import { Body, SubTitle, CarouselCard, TitleBackCard } from '../../Components/Mix/Main'
import { ScrollView, Image } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import { BigAvatar, Card, Button, Center, Title, H2, H1, Wrap } from '../../Components/Root/Main';
import * as Style from '../../Styles/Style';


const AlmacenScreen = () => {
    const Navigate = Screens();
    
    

    return (
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Almacen</TitleBackCard>
            <ScrollView>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 150}]}>
                        <Title style={{marginBottom:10}}>Modelos</Title>
                       
                        <Button onPress={Navigate.Modelos}>
                          <H1>VER MÁS</H1>
                        </Button>
                    </Card>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 150}]}>
                        <Title style={{marginBottom:10}}>Piezas</Title>
                       
                        <Button onPress={Navigate.Piezas}>
                          <H1>VER MÁS</H1>
                        </Button>
                    </Card>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 150}]}>
                        <Title style={{marginBottom:10}}>Materiales</Title>
                       
                        <Button onPress={Navigate.Materiales}>
                          <H1>VER MÁS</H1>
                        </Button>
                    </Card>
        
              
            </ScrollView>
        </Body>
    )
}

export default AlmacenScreen;