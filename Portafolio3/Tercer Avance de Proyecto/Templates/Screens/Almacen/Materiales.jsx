import { Body, SubTitle, CarouselCard, TitleBackCard, InfoCard } from '../../Components/Mix/Main'
import { InfoCardMateriales } from '../../Components/Mix/Home/InfoCard'
import { ScrollView, Image } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import { BigAvatar, Card, Button, Center, Title, H2, H1, Wrap } from '../../Components/Root/Main';
import * as Style from '../../Styles/Style';
import { GET } from '../../../Core/Connection/Global';




const MaterialesScreen = () => {       //AQUI
    const Navigate = Screens();
    const Materiales = GET('materiales');

    return (
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Almacen</TitleBackCard>
            <ScrollView>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 150}]}>
                <Title style={{marginBottom:10}}>Materiales</Title>
                <Title style={{marginBottom:10,alignSelf: "flex-start"}} >Aqui puedes encontrar todos los materiales que necesitas para fabricar piezas.</Title>
                        
            </Card>
            <InfoCardMateriales style={{width: 150}} ImgStyle={{flex: 0, height: "50%"}} list={Materiales}/>

              
            
            </ScrollView>
        </Body>
    )
}

export default MaterialesScreen;