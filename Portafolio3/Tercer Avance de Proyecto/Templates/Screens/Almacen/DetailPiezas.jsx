import { Body, TitleBackCard, TitleCard, Center} from '../../Components/Mix/Main';
import { BigAvatar, Card, Title, Wrap, } from '../../Components/Root/Main';
import { ScrollView } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Style from '../../Styles/Style';
import { GETnumero } from '../../../Core/Connection/Global';
import * as Theme from '../../Styles/Theme';
import { View } from 'react-native-animatable';

const DetailPiezaScreen = ({ route}) => {
    const Navigate = Screens();
    const { pieza } = route.params;
   // const detail = GETnumero('detailPieza/', pieza.numero);

    // Verificar si la data está disponible y si contiene el índice "numero"
   // const detailData = detail && detail.length > 0 && detail[0].numero === pieza.numero ? detail[0] : null;

    return (
        <Body style={{backgroundColor: Theme.color.subFondo }}>
            <TitleBackCard onPress={Navigate.Back}>Detalles</TitleBackCard>
            <ScrollView>
                <Wrap>
                    <Card style={[Style.PresentationCard, { height: 250 }]}>
                        <Center>
                            <BigAvatar url={pieza ? pieza.url : ""} />
                        </Center>
                    </Card>
                    <Title style={{ color: Theme.color.texto.normal, alignSelf: "flex-start" }}>
                        Nombre: 
                    </Title>
                    <TitleCard 
                    style={{height:50}}

                    styleText={{
                        fontSize: 22 
                    }}>
                        {pieza ? pieza.nombre : ""}
                    </TitleCard>
                    <Card style={[Style.TopCard, {paddingHorizontal: 0,backgroundColor: Theme.color.tertiary, justifyContent: 'space-between', height: 130 }]}>
                        <Title style={{alignSelf: "center" }}>
                            Descripción: 
                        </Title>
                        <Card style={[Style.TopCard, {
                            marginBottom: 0,
                            backgroundColor: Theme.color.fourth, 
                            justifyContent: 'space-between', 
                            height: 50, 
                            flex: 1,
                            borderWidth: 0,
                            borderColor: 'transparent',
                            elevation: 0,
                            margin: 0,
                            borderRadius: 12,
                            alignItems: 'center',
                            }]}>
                                <Center>
                        <Title style={{color: Theme.color.texto.normal,alignSelf: "center"}}>
                            {pieza ? pieza.descripcion : ""}
                        </Title>
                        </Center>
                        </Card>
                    </Card>                
                </Wrap>
                <Title style={{color: Theme.color.texto.normal,alignSelf: "center"}}>
                        Stock:
                    </Title>

                    <Card style={[Style.TopCard, {
                            marginBottom: 0,
                            backgroundColor: Theme.color.fourth, 
                            justifyContent: 'space-between', 
                            height: 70, 
                            width: 170,
                            flex: 1,
                            borderWidth: 0,
                            borderColor: 'transparent',
                            elevation: 0,
                            margin: 0,
                            borderRadius: 12,
                            alignItems: 'center',
                            alignSelf: 'center',
                            
                            }]}>
                        <Title style={{color: Theme.color.texto.normal, alignSelf: "center", fontSize: 22, marginTop: 20}}>
                            {pieza ? pieza.stock : ""}
                        </Title>
                    </Card>
            </ScrollView>
        </Body>
    );
}

export default DetailPiezaScreen;