import { Body, SubTitle, CarouselCard, TitleBackCard, InfoReport, Button, H1, Card, Title, Center} from '../../Components/Mix/Main'
import { ScrollView } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Style from '../../Styles/Style';
import { GET } from '../../../Core/Connection/Global';

const ProcesosScreen = () => {
    const Navigate = Screens();
    const Reportes = GET('reportes')
    return (
        <Body>
            <TitleBackCard onPress={ Navigate.Back }>Reportes</TitleBackCard>
                <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 180}]}>
                    <Center>
                    <Title>Si ha ocurrido algún evento durante el transcurso las labores sera documentado y aparecera en esta pantalla:</Title>
                
               
                    </Center>
                </Card>

            <ScrollView>
            <InfoReport list={Reportes}/>
            </ScrollView>
        </Body>
    )
}

export default ProcesosScreen;