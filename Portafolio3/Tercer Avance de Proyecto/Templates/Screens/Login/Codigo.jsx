import React, { useState } from 'react';
import Animated, { BounceInUp, BounceInRight, LightSpeedInLeft } from 'react-native-reanimated';
import { Text, TextInput, View, TouchableOpacity, Alert} from 'react-native';

import { Body, Card, Center, Icon, Title, SubTitle, TitleBackCard } from '../../Components/Mix/Main' 
import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from 'expo-linear-gradient';

import { IngresarCodigo } from '../../../Core/Connection/Login';
import { Screens } from '../../../Core/Mains/NavigatorMain';
import { color, sizes, width } from '../../Styles/Theme';


const CodigoScreen = () => {
    const Navigate = Screens();
  const [codigo, setcodigo] = useState('');

  async function handleLogin() {
  try {
    const data = await IngresarCodigo(codigo);
    Alert.alert('Código Correcto');
    Navigate.Password();
  } catch (error) {
    console.log(error);
    Alert.alert('Error', error.message);
  }
}


  return (
    <View style={{backgroundColor: '#2D3154'}}>
      <View style={{width: '100%', height: '38%'}}>
        <Center>
        <TitleBackCard onPress={Navigate.Back}>Restablecer contraseña</TitleBackCard>
          <Card style={{width:125, height: 125, backgroundColor: "#FFFFFF", borderRadius: 25}}>
            <Center>
            <Icon icon="Logo" size={125} style={{tintColor: "#2D3154"}}/>
            </Center>
          </Card>
        </Center>
      </View >
      <View style={{width: '100%', height: '62%', backgroundColor: "#FFFFFF", borderTopRightRadius: 65, borderTopLeftRadius: 10}}>
        <Center>
          <Title style={{color: '#000000', fontSize: 40, marginBottom: 20}}>OPPENLAB</Title>
          <View style={{width: 270, alignSelf: 'auto'}}>

            <SubTitle>Ingresar código de seguridad:</SubTitle> 
            <Animated.View entering={LightSpeedInLeft.delay(350)} className=" bg-black/5 p-3 mt4 rounded-2xl w-full">
              <TextInput placeholder='Código' value={codigo} onChangeText={setcodigo} placeholderTextColor={'gray'}/>
            </Animated.View>

            <Animated.View entering={LightSpeedInLeft.delay(500)} className="w-full">
                <TouchableOpacity className=" p-3 w-full bg-[#2D3154] rounded-2xl mt-5 mb-5" onPress={handleLogin}>
                  <Text className="text-xl font-bold text-center text-white">Ingresar Código</Text>
                </TouchableOpacity>
              </Animated.View>

          </View>
        </Center>
      </View>
    </View>
  );
}


export default CodigoScreen;