import { Screens } from "../../../Core/Mains/NavigatorMain";
import { View, ActivityIndicator } from 'react-native';
import { useEffect } from "react";



const Loader = ({ onFinish }) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      onFinish();
    }, 1);
    return () => clearTimeout(timer);
  }, [onFinish]);
  
  return <ActivityIndicator size="large" color="#0000ff" />;
};

const LoadingScreen = () => {
  const Navigate = Screens();
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Loader onFinish={Navigate.Tab} />
    </View>
  );
};

export default LoadingScreen;