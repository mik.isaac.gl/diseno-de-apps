import React, { useState } from 'react';
import Animated, { BounceInUp, BounceInRight, LightSpeedInLeft } from 'react-native-reanimated';
import { Text, TextInput, View, TouchableOpacity, Alert} from 'react-native';

import { Body, Card, Center, Icon, Title, SubTitle, TitleBackCard, Button } from '../../Components/Mix/Main' 
import * as Theme from '../../Styles/Theme';

import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from 'expo-linear-gradient';

import { solicitarCodigo } from '../../../Core/Connection/Login';
import { Screens } from '../../../Core/Mains/NavigatorMain';
import { color, sizes, width } from '../../Styles/Theme';

const ForgotPasswordScreen = () => {
const Navigate = Screens();
  const [correo, setcorreo] = useState('');

  async function handleLogin() {
  try {
    const data = await solicitarCodigo(correo);
    Alert.alert('Código enviado');
    Navigate.Codigo();
  } catch (error) {
    console.log(error);
    Alert.alert('Error', error.message);
  }
}


  return (
    <View style={{backgroundColor: '#2D3154'}}>
      <View style={{width: '100%', height: '38%'}}>
        <Center>
          <Card style={{width:125, height: 125, backgroundColor: "#FFFFFF", borderRadius: 25}}>
            <Center>
            <Icon icon="Logo" size={125} style={{tintColor: "#2D3154"}}/>
            </Center>
          </Card>
        </Center>
      </View >
      <View style={{width: '100%', height: '62%', backgroundColor: "#FFFFFF", borderTopRightRadius: 65, borderTopLeftRadius: 10}}>
        <Center>
          <Title style={{color: '#000000', fontSize: 40, marginBottom: 15}}>OPPENLAB</Title>
          <View style={{width: 300, alignSelf: 'auto'}}>

            <SubTitle>correo:</SubTitle> 
            <Animated.View entering={LightSpeedInLeft.delay(350)} className=" bg-black/5 p-3 mt4 rounded-2xl w-full">
              <TextInput placeholder='Correo' value={correo} onChangeText={setcorreo} placeholderTextColor={'gray'}/>
            </Animated.View>

            <Animated.View entering={LightSpeedInLeft.delay(500)} className="w-full">
              <TouchableOpacity className=" p-3 w-full bg-[#2D3154] rounded-2xl mt-5 mb-5" onPress={handleLogin}>
                <Text className="text-xl font-bold text-center text-white">Enviar Código De Seguridad</Text>
              </TouchableOpacity>
            </Animated.View>

          </View>
          <Button onPress={Navigate.Back} style={{}}>
            <Icon icon="Back" size={20} style={{tintColor: Theme.color.iconos}}></Icon>
          </Button>
        </Center>
        
      </View>
    </View>
  );
}


export default ForgotPasswordScreen;