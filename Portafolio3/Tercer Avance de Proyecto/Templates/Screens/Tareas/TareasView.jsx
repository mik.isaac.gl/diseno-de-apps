 import { Body, TitleBackCard, H1, H2, Center, Title, Card, CarouselCard, AccordionItem} from '../../Components/Mix/Main'
import { ScrollView, Text, View } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Theme from '../../Styles/Theme';
import * as Style from '../../Styles/Style';

const TareasView = () => {
    const Navigate = Screens();
    const list = 
        {
            numero: 1,
            descripcion: "Seguir protocolos de seguridad en la manipulación de químicos",
            estado: 1,
            empleado: 1,
            area: 1
        };
        
    return (
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Tarea</TitleBackCard>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: "100%"}]}>
                <Title style={{marginBottom:25}}>Detalles</Title>
                <Title style={{marginBottom:25,alignSelf: "flex-start"}} >Estado: {list.estado}</Title>
                <Title style={{marginBottom:25,alignSelf: "flex-start"}} >Descripción: {list.descripcion}</Title>
                <Title style={{marginBottom:25,alignSelf: "flex-start"}} >Empleados: {list.empleados}</Title>
                <Title style={{marginBottom:25,alignSelf: "flex-start"}} >Área de Producción: {list.area}</Title>
            </Card>
        </Body>
    )
}


export default TareasView;