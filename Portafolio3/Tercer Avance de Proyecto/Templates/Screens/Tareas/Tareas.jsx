import { Body, TitleBackCard, InfoCardTareas, TitleCard, Button, Card, Title, H1, SubInfo} from '../../Components/Mix/Main'
import { ScrollView } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Style from '../../Styles/Style';
import { GET } from '../../../Core/Connection/Global';


const TareasScreen = () => {
    const Navigate = Screens();
    const area = GET('areasProduccion');

    return (
        <Body>
            <TitleBackCard onPress={ Navigate.Back }>Tareas</TitleBackCard>
            <ScrollView >
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 110}]}>
                        <Title style={{marginBottom:10}}>Mira todas las tareas que hay en las áreas de producción</Title>
                       
                        <Button onPress={Navigate.AsignarTarea}>
                          <H1>Asignar nueva tarea</H1>
                        </Button>
                    </Card>
            <InfoCardTareas style={{width: 155}} list={area}/>
            </ScrollView>
        </Body>
    )
}

export default TareasScreen;
