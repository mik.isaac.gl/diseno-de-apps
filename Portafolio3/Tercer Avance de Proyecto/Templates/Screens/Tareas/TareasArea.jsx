import { Body, TitleBackCard, SubInfo, Title, CardTouch, Center, TitleCard, InfoTask} from '../../Components/Mix/Main'
import { ScrollView } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Theme from '../../Styles/Theme';
import * as Style from '../../Styles/Style';
import { Wrap } from '../../Components/Root/Main';
import { GETnumero } from '../../../Core/Connection/Global';

const TareasAreaScreen = ({route}) => {
    const Navigate = Screens();
    const { area } = route.params;
    const detail = GETnumero('tareasArea/', area.numero);

    return (
        <Body> 
            <TitleBackCard onPress={Navigate.Back}>Tareas</TitleBackCard>
            
                <ScrollView>
                    <TitleCard style={{height: 60}}> {area.nombre}</TitleCard>
                    <InfoTask list={detail}></InfoTask>
                </ScrollView>
            
        </Body>
    );
}

export default TareasAreaScreen;