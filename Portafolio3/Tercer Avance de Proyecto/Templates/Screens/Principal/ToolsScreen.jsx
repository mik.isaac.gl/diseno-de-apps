import { UserCard, Body,CarouselList, SubTitle, InfoTask, TareasRR,  TitleCard, Button, H1 } from "../../Components/Mix/Main.jsx";
import { ScrollView } from "react-native";
import { Screens } from '../../../Core/Mains/NavigatorMain';
import { GETuser } from "../../../Core/Connection/Global.js";

const ToolsScreen = () => {  
    const Navigate = Screens();
    const Menus = [
      {
        numero: 1,
        icon: "Employees",
        title: "Empleados",
        navigate: Navigate.Empleados,
      },
      {
        numero: 3,
        icon: "Task",
        title: "Tareas",
        navigate: Navigate.Tareas,
      },
      {
        numero: 4,
        icon: "Pendiente",
        title: "Reportes",
        navigate: Navigate.Procesos,
      },
      {
        numero: 5,
        icon: "Storage",
        title: "Almacen",
        navigate: Navigate.Almacen,
      }, 
    ];
    
    const Tareas = GETuser('tareasEmpleado/');
    const User = GETuser('perfil/');
    

    return(
        <Body>
          <TitleCard>Herramientas</TitleCard>
          <ScrollView>
            <UserCard item={User}/>
            <SubTitle>Herramientas</SubTitle>
            <CarouselList list={Menus}/>
            <SubTitle>Mis Tareas  </SubTitle>
            <TareasRR list={Tareas}></TareasRR>
          </ScrollView>
        </Body>
    )
};


export default ToolsScreen;