import { Body, SubTitle, CarouselCard, InfoCard, TitleCard } from '../../Components/Mix/Main'
import { ScrollView } from 'react-native';
import { GET, useGET } from '../../../Core/Connection/Global';
import * as Theme from '../../Styles/Theme';
import { useIsFocused } from '@react-navigation/native';


const HomeScreen = () => {
    const isFocused = useIsFocused();
    const ruta = 'areasProduccion';
    const areasTrabajo = GET(ruta);

    const ruta2 = 'dispositivoDht';
    const Dispositivos = useGET(ruta2, isFocused);


    return(
        <Body>
            <TitleCard>Home</TitleCard>
              <ScrollView showsVerticalScrollScrollViewIndicator={false}>
              <CarouselCard list={Dispositivos} />
              <SubTitle>Áreas de Trabajo</SubTitle>
              <InfoCard style={{width:Theme.width-40}} list={areasTrabajo}/>
            </ScrollView>
        </Body>
    )
};

export default HomeScreen;