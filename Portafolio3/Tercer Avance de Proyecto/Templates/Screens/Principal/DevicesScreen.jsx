import { Body,DevicesCard, DevicesControl, TitleCard } from "../../Components/Mix/Main.jsx";
import { ScrollView } from "react-native";
import { useGET } from "../../../Core/Connection/Global.js";
import React from 'react';
import { useIsFocused } from '@react-navigation/native';

const DevicesScreen = () => {
    const isFocused = useIsFocused();
    const temperatura = useGET('temperatura', isFocused);
    const humedad = useGET('humedad', isFocused);

    return(
        <Body>
            <ScrollView>
            <DevicesControl item={temperatura}></DevicesControl>
            <DevicesCard item={humedad}></DevicesCard>
            </ScrollView>
        </Body>
    )
};

export default DevicesScreen;