import { Body, TitleBackCard, SubInfo, Title, CardTouch, Center, TitleCard, InfoEmployee} from '../../Components/Mix/Main'
import { ScrollView } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Theme from '../../Styles/Theme';
import * as Style from '../../Styles/Style';
import { GETnumero } from '../../../Core/Connection/Global';

const EmpleadosAreaScreen = ({route}) => {
    const Navigate = Screens();
    const { area } = route.params;
    const detail = GETnumero('empleadosArea/', area.numero);

    return (
        <Body> 
            <TitleBackCard onPress={Navigate.Back}>Empleados</TitleBackCard>
                <ScrollView>
                    <TitleCard style={{height: 60}}> {area.nombre}</TitleCard>
                    <InfoEmployee list={detail}></InfoEmployee>
                </ScrollView>
        </Body>
    );
}

export default EmpleadosAreaScreen;