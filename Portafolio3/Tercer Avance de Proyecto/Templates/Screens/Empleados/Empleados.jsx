import { Body, InfoCard, InfoCardEmpleadosPro, InfoCarousel, SubTitle, TitleBackCard, Button } from "../../Components/Mix/Main"
import { ScrollView, View } from "react-native";
import { Screens } from "../../../Core/Mains/NavigatorMain";
import { empleados } from "../../../Core/Connection/rutas/Empleados/empleados";
import { jefes } from "../../../Core/Connection/rutas/Empleados/jefes";
import { GET } from '../../../Core/Connection/Global';

const EmpleadosScreen = () => {
    const Navigate = Screens();
    
    const Empleados = GET ('areasProduccion');
    const Jefes = jefes();
    
    return(
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Empleados</TitleBackCard>
            <ScrollView showsVerticalScrollScrollViewIndicator={false}>
              <InfoCarousel list={Jefes}></InfoCarousel>
                              <SubTitle>Áreas de Empleados</SubTitle>

                <InfoCardEmpleadosPro list={Empleados}></InfoCardEmpleadosPro>

            </ScrollView>
        </Body>
    )
}

export default EmpleadosScreen;