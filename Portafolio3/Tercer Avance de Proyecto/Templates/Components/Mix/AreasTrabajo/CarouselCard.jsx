import { Card, Carousel, ErrorCard } from '../../Root/Main';
import { View } from 'react-native';
import * as Style from '../../../Styles/Style';
import { DevicesCard } from '../Dispositivos/DevicesCard';

function isEmpty(item) {
    if (item == null || item.length == 0 ){
        return true;
    }
    return false;
    }

const CarouselCard = ({list, style, styleItem, children}) => {
      try{
        if (isEmpty(list)) {
          return (
            <ErrorCard style={Style.Device.vista}></ErrorCard>
          );
        }
        return(
            <View>
                <Carousel items={list} styleItem={Style.CarrouselCard.static}
                    renderItem={({item, style}) => {
                    try {  
                        if (isEmpty(item) || item.data == null) {
                            return (
                              <ErrorCard style={Style.Device.vista}></ErrorCard>
                            );
                          }
                        return (
                            <DevicesCard key={item.numero} style={style} item={item}></DevicesCard>
                        );  
                    } catch (error) {
                       return(
                        <ErrorCard style={Style.Device.vista}></ErrorCard>
                       ); 
                    }
                    
                }}/>
            </View>
        )
      }catch{
        return (
            <ErrorCard style={Style.Device.vista}></ErrorCard>
          );
      }
       
};
export default CarouselCard;  


