import { Title, Card, Center } from "../../Root/Main";
import * as Style from "../../../Styles/Style"
import * as Theme from "../../../Styles/Theme"

export const TitleCard = ({children, style, }) => {
    return(
        <Card style={[Style.TitleCard, style]}>
            <Center>
                <Title >{children}</Title>
            </Center>
        </Card>
    )
}