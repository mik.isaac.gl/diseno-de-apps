import {CardGraphic, Carousel, Card} from "../Root/Main";

const TopGraphic = ({list}) => {
    const navigation = useNavigation();
    <Carousel
      items={list}
      renderItem={({item, style}) => {
        return (
          <Card
            style={[componentsStyle.card, style]}
            onPress={() => {
              navigation.navigate('Details', {trip: item});
            }}>
              <CardGraphic source={item.image} borderBottomRadius />
          </Card>
        );
      }}
    />
}

export default TopGraphic;