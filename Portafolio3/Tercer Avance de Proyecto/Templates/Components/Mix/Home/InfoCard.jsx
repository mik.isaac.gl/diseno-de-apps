import React from 'react';
import { CardTouch, Media, CardContent, Wrap, InfoTitle, SubInfo, ErrorCard } from '../../Root/Main';
import * as Style from '../../../Styles/Style';
import { useState } from 'react';
import { Screens } from '../../../../Core/Mains/NavigatorMain';

const def = [
  {
    id: 1,
    title: 'Cappadocia',
    subtitle: 'Turkey',
    
  },
];

function isEmpty(item=[]) {
  if ( item == null || item.length == 0) {
    return true;
  }
  return false;
}

export const InfoCard = ({list = [], style, ImgStyle}) => {

  if (isEmpty(list)) {
    return (<ErrorCard style={{height:300}}></ErrorCard>);
  }
try{
  return (
    <Wrap>
      {list.map((item, index) => (
        <CardTouch key={item.numero} style={[Style.InfoCard, style, ]}>
          <Media source={item.url} style={ImgStyle}/>
          <CardContent>
            <InfoTitle>{item.nombre}</InfoTitle>
            {/* <SubInfo>{item.subtitle}</SubInfo> */}
          </CardContent>
        </CardTouch>
      ))}
    </Wrap>
  );
}catch{
  return (<ErrorCard style={{height:300}}></ErrorCard>);
}
  
};

export const InfoCardTareas = ({list, style, ImgStyle}) => {
  const navigate = Screens();
  if (isEmpty(list)) {
    return (<ErrorCard style={{height:300}}></ErrorCard>);
  }

  try {
    return (
      <Wrap>
        {list.map((item, index) => {
          return(
          <CardTouch key={item.numero} onPress={() => navigate.TareasArea(item)} style={[Style.InfoCard, style]}>
            <Media source={item.url} style={ImgStyle}/>
            <CardContent>
              <InfoTitle>{item.nombre}</InfoTitle>
              {/* <SubInfo>{item.subtitle}</SubInfo> */}
            </CardContent>
          </CardTouch>
          )
        })}
      </Wrap>
    );
  } catch (error) {
    return (
      <ErrorCard style={{height:300}}></ErrorCard>
    );
  }

  
};

export const InfoCardTareasArea = ({list, style, ImgStyle}) => {
  const navigate = Screens();
  if (isEmpty(list)) {
    return <ErrorCard style={{height:300}}></ErrorCard>;
  }

  return (
    <Wrap>
      {list.map((item, index) => {
        return(
        <CardTouch key={item.numero} onPress={() => navigate.TareasArea(item)} style={[Style.InfoCard, style]}>
          <Media source={item.url} style={ImgStyle}/>
          <CardContent>
            <InfoTitle>{item.nombre}</InfoTitle>
            {/* <SubInfo>{item.subtitle}</SubInfo> */}
          </CardContent>
        </CardTouch>
        )
      })}
    </Wrap>
  );
};

export const InfoCardEmpleadosPro = ({list, style, ImgStyle}) => {
  const navigate = Screens();
  if (isEmpty(list)) {
    return (<ErrorCard style={{height:300}}></ErrorCard>);
  }

  try {
    return (
      <Wrap>
        {list.map((item, index) => {
          return(
          <CardTouch key={item.numero} onPress={() => navigate.EmpleadosArea(item)} style={[Style.InfoCard, style]}>
            <Media source={item.url} style={ImgStyle}/>
            <CardContent>
              <InfoTitle>{item.nombre}</InfoTitle>
              {/* <SubInfo>{item.subtitle}</SubInfo> */}
            </CardContent>
          </CardTouch>
          )
        })}
      </Wrap>
    );
    
  } catch (error) {
    return (
      <ErrorCard style={{height:300}}></ErrorCard>
    );
  }

  
};


export const InfoCardEmpleados = ({list, style, ImgStyle}) => {
  const navigate = Screens()
  if (isEmpty(list)) {
    return (
      <ErrorCard style={{height:300}}></ErrorCard>
    );
  }

  return (
    
    <Wrap>
      {list.map((item, index) => {
        return (
          <CardTouch key={item.numero} style={[Style.InfoCard, style]}>
            <Media source={item.image} style={ImgStyle}/>
            <CardContent>
                <InfoTitle >{item.title}</InfoTitle>
                <SubInfo>{item.subtitle}</SubInfo>
            </CardContent>
          </CardTouch>
        );
      })}
    </Wrap>
  );
};


export const InfoCardModelos = ({list, style, ImgStyle}) => {
  const navigate = Screens();
  try{
    if (isEmpty(list)) {
      return (
        <ErrorCard style={{height:300}}></ErrorCard>
      );
    }
    return (
      
      <Wrap>
        {list.map((item, index) => {
          return (
            <CardTouch key={item.numero} onPress={() => navigate.DetailModelos(item)} style={[Style.InfoCard, style]}>
              <Media source={item.url} style={ImgStyle}/>
              <CardContent>
                  <InfoTitle >{item.nombre}</InfoTitle>
                  <SubInfo>Stock: {item.stock}</SubInfo>
              </CardContent>
            </CardTouch>
          );
        })}
      </Wrap>
    );
  } catch{
    return (
      <ErrorCard style={{height:300}}></ErrorCard>
    );
  }
};

export const InfoCardPiezas = ({list, style, ImgStyle}) => {
  const navigate = Screens();
  try {
    if (isEmpty(list)) {
      return (
        <ErrorCard style={{height:300}}></ErrorCard>
      );
    }
    return (
      <Wrap>
        {list.map((item, index) => {
          return (
            <CardTouch key={item.numero} onPress={() => navigate.DetailPieza(item)} style={[Style.InfoCard, style]}>
              <Media source={item.url} style={ImgStyle}/>
              <CardContent>
                  <InfoTitle >{item.nombre}</InfoTitle>
                  <SubInfo>Stock: {item.stock}</SubInfo>
              </CardContent>
            </CardTouch>
          );
        })}
      </Wrap>
    );
  } catch {
    return (
      <ErrorCard style={{height:300}}></ErrorCard>
    );
  }
};

export const InfoCardMateriales = ({list, style, ImgStyle}) => {
  const navigate = Screens();
  
 try {
  
  if (isEmpty(list)) {
    return (
      <ErrorCard style={{height:300}}></ErrorCard>
    );
  }
  return (
    <Wrap>
      {list.map((item, index) => {
        return (
          <CardTouch key={item.numero} onPress={() => navigate.DetailMaterial(item)} style={[Style.InfoCard, style]}>
            <Media source={item.url} style={ImgStyle}/>
            <CardContent>
                <InfoTitle >{item.nombre}</InfoTitle>
                <SubInfo>Stock: {item.stock}</SubInfo>
            </CardContent>
          </CardTouch>
        );
      })}
    </Wrap>
  );
 } catch {
  return (
    <ErrorCard style={{height:300}}></ErrorCard>
  );
 }
};