import {Horizontal, Icon, Button, Card,} from "../../Root/Main"
import * as Theme from "../../../Styles/Theme";
import * as Style from "../../../Styles/Style"
import { TitleCard } from "../AreasTrabajo/TitleCard";
export const TitleBackCard = ({children, onPress, animation}) => {
    return(
        <Card style={[Style.PresentationCard, {height: "8%"}]}>
        <Horizontal style={{marginBottom: "5%"}}>
            <Button onPress={onPress} style={{borderRadius: 16, marginRight: 2,height: 40, width:"25%"}}>
                <Icon icon="Back" size={20} style={{tintColor: Theme.color.iconos}}/>
            </Button>
            <TitleCard style={{marginBottom: "0%",height: '100%', width: "75%"}}>{children}</TitleCard>
        </Horizontal>
        </Card>
    )
}