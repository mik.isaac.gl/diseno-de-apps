import { Card, H3, Center, H1,H2, Horizontal, Icon, CardTouch, ErrorCard } from "../../Root/Main"
import { View } from "react-native"
import * as Style from "../../../Styles/Style"
import * as Theme from "../../../Styles/Theme"
import { POST } from "../../../../Core/Connection/Global"

function isEmpty(item) {
    if (item == null || item.length == 0 || item.data == null) {
        return true;
    }
    return false;
}



export const DevicesControl = ({style, item}) => {
    
    function MaxMas(){
        POST('temperatura', {"tempMax":item.max + 1, "tempMin":item.min})
    }
    
    function MinMas(){
        POST('temperatura', {"tempMax":item.max, "tempMin":item.min+1})
    }
    
    function MaxMenos(){
        POST('temperatura', {"tempMax":item.max - 1, "tempMin":item.min})
    }
    
    function MinMenos(){
        POST('temperatura', {"tempMax":item.max, "tempMin":item.min-1})
    }

    
    
    if (isEmpty(item)) {
        return (
          <ErrorCard style={Style.Device.control}></ErrorCard>
        );
    }

    try{
        return (
            <Card style={[Style.Device, Style.Device.control, style, ]}>
    
                <Card style={[Style.SubCard, {height: "40%"}]}>
                    <Center>
                        <Horizontal>
                            <H3 style={{fontSize: 55, marginRight: 10, marginLeft:20}}>{item.data}</H3>
                            <Icon icon={item.nombre} size={100} style={{tintColor: Theme.color.iconos}}/>
                        </Horizontal>
                    </Center>
                </Card>
    
                <H1 style={{fontSize: 20, marginBottom: 10}}>Ajustar limites</H1>
    
                <View style={{height: "34%", }}>
                    <Horizontal style={{justifyContent: 'space-between'}}>
                        <CardTouch onPress={MaxMenos} style={[Style.buttonCard, style, {backgroundColor:Theme.color.tertiary,borderColor: Theme.color.bordes.color2,backgroundColor:"#4B7583",width: 40, height:40}]}>
                            <View style={Style.center}>
                                <Icon icon="Menos" size={10} style={{tintColor: Theme.color.iconos}}/>
                            </View>
                        </CardTouch>
    
                        <H3 style={{fontSize: 25}}>Max: {item.max}°C</H3>
    
                        <CardTouch onPress={MaxMas} style={[Style.buttonCard, style, {backgroundColor:Theme.color.tertiary,borderColor: Theme.color.bordes.color2,backgroundColor:"#4B7583",width: 40, height:40}]}>
                            <View style={Style.center}>
                                <Icon icon="Mas" size={10} style={{tintColor: Theme.color.iconos}}/>
                            </View>
                        </CardTouch>
    
                    </Horizontal>
    
                    <Horizontal style={{justifyContent: 'space-between'}}>
                        <CardTouch onPress={MinMenos} style={[Style.buttonCard, style, {backgroundColor:Theme.color.tertiary,borderColor: Theme.color.bordes.color2,width: 40, height:40}]}>
                            <View style={Style.center}>
                                <Icon icon="Menos" size={10} style={{tintColor: Theme.color.iconos}}/>
                            </View>
                        </CardTouch>
    
                        <H3 style={{fontSize: 25}}>Min: {item.min}°C</H3>
    
                        <CardTouch onPress={MinMas} style={[Style.buttonCard, style, {backgroundColor:Theme.color.tertiary,borderColor: Theme.color.bordes.color2,width: 40, height:40}]}>
                            <View style={Style.center}>
                                <Icon icon="Mas" size={10} style={{tintColor: Theme.color.iconos}}/>
                            </View>
                        </CardTouch>
    
                    </Horizontal>
                </View>
    
                <View style={{height: "10%"}}>
                    <Center>
                        <H1 style={{fontSize: 25}}>{item.nombre}</H1>
                    </Center>
                </View>
                
            </Card>
        )
    }catch{
        return (
            <ErrorCard style={Style.Device.control}></ErrorCard>
        );
    }

    
}