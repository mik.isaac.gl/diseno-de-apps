import { Card, H3, Center, H1,H2, Horizontal, Icon, CardTouch, ErrorCard } from "../../Root/Main"
import { View } from "react-native"
import * as Style from "../../../Styles/Style"
import * as Theme from "../../../Styles/Theme"


function isEmpty(item) {
    if (item == null || item.length == 0 || item.data == null ) {
        return true;
    }
    return false;
}


export const DevicesCard = ({style, children, item}) => {

    if (isEmpty(item)) {
        return (
          <ErrorCard style={Style.Device.vista}></ErrorCard>
        );
      }

    try{
        return (
            <Card style={[Style.Device, Style.Device.vista, style]}>
                <Card style={[Style.SubCard]}>
                    <Center>
                        <Horizontal>
                            <H3 style={{fontSize:40,marginRight: 10}}>{item.data}%</H3>
                            <Icon icon={item.nombre} size={70} style={{tintColor: Theme.color.iconos}}/>
                        </Horizontal>
                    </Center>   
                </Card>
                <View style={{height: "30%"}}>
                    <Center>
                        <H1 style={{fontSize: 25}}>{item.nombre}</H1>
                    </Center>
                </View>
            </Card>
        )
    }catch{
        return (
            <ErrorCard style={Style.Device.vista}></ErrorCard>
          );
    }
    
    
}