import React from 'react';
import { Carousel} from '../../Root/Main';
import { BtnIconText } from './BtnIconText';
import * as Style from '../../../Styles/Style';
import { View } from 'react-native-animatable';


const CarouselList = ({list}) => {
  return (
    <View>
      <Carousel items={list} styleItem={Style.CarouselList.static}
        renderItem={({item, style}) => {
          return (
            <BtnIconText key={list.numero} onPress={item.navigate} style={style} icon={item.icon}>{item.title}</BtnIconText>
          );
      }}/>
    </View>
  );
};

export default CarouselList;