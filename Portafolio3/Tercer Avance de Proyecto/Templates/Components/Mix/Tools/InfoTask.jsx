import React from 'react';
import { CardTouch, CardContent, AccordionItem ,Wrap, InfoTitle, SubInfo, Horizontal,Media, ErrorCard, Button, Icon, Center, H1 } from '../../Root/Main';
import * as Style from '../../../Styles/Style';
import { Screens } from '../../../../Core/Mains/NavigatorMain';
import * as Theme from '../../../Styles/Theme';

function isEmpty(item =[]) {
  if (item.length == 0) {
      return true;
  }
  return false;
  }


export const InfoTask = ({list}) => {
  if (isEmpty(list)) {
    return (
      <ErrorCard style={Style.Device.vista}></ErrorCard>
    );
  }

try{
  return (
    <Wrap>
      
      {list.map((item, index) => {
        
        return (
          <AccordionItem key={item.numero} style={[Style.InfoCard, Style.InfoCard.horizontal, { marginBottom:0}]}
          content={
            <CardContent>
              <Center>
              <Horizontal>
                <Icon icon='Pendiente' size={50} style={{tintColor: Theme.color.iconos}}></Icon>
                <CardContent>
                <InfoTitle>Tarea número {item.numero}</InfoTitle>
                <SubInfo>{item.descripcion}</SubInfo>
                </CardContent>
              </Horizontal>
              </Center>
            </CardContent>
          }>
            <CardContent>
              <InfoTitle>Estado: <SubInfo>{item.estado}</SubInfo></InfoTitle>
              <InfoTitle>Área de trabajo: <SubInfo>{item.areas}</SubInfo></InfoTitle>
              <InfoTitle>Trabajador: <SubInfo>{item.empleado}</SubInfo></InfoTitle>
              <InfoTitle>Reportes: <SubInfo>{item.reportes}</SubInfo></InfoTitle>
            </CardContent> 
          </AccordionItem>
        );
      })}
      
    </Wrap>
  );
} catch {
  return (
    <ErrorCard style={Style.Device.vista}></ErrorCard>
  );
}
};

export const InfoTaskReporte = ({list}) => {
  if (isEmpty(list)) {
    return (
      <ErrorCard style={Style.Device.vista}></ErrorCard>
    );
  }

try{
  return (
    <Wrap>
      
      {list.map((item, index) => {
        
        return (
          <AccordionItem key={item.numero} style={[Style.InfoCard, Style.InfoCard.horizontal, { width:350, marginBottom:0}]}
          content={
            <CardContent>
              <Center>
              <Horizontal>
                <CardContent>
                <InfoTitle>Tarea número {item.numero}</InfoTitle>
                <SubInfo>{item.descripcion}</SubInfo>
                
                </CardContent>
              </Horizontal>
              </Center>
            </CardContent>
          }>
          </AccordionItem>
        );
      })}
      
    </Wrap>
  );
} catch {
  return (
    <ErrorCard style={Style.Device.vista}></ErrorCard>
  );
}
};

export const InfoEmployee = ({list}) => {
  const Navigate = Screens();

  if (isEmpty(list)) {
    return (
      <ErrorCard style={Style.Device.vista}></ErrorCard>
    );
  }

try{
  return (
    <Wrap>
      
      {list.map((item, index, ImgStyle) => {
        
        return (
            <CardTouch key={index} style={[Style.InfoCard]}>
              <Media source={item.url} style={ImgStyle}/>
              <CardContent>
                <InfoTitle>{item.empleado}</InfoTitle>
                <SubInfo>{item.correo}</SubInfo> 
                {/* <SubInfo>{item.}</SubInfo> */}
              </CardContent>
            </CardTouch>
        );
      })}
      
    </Wrap>
  );
} catch{
  return (
    <ErrorCard  style={Style.Device.vista}></ErrorCard>
  );
}
};


export const InfoReport = ({list}) => {
  if (isEmpty(list)) {
    return (
      <ErrorCard style={Style.Device.vista}></ErrorCard>
    );
  }

try{
  return (
    <Wrap>
      
      {list.map((item, index) => {
        
        return (
          <AccordionItem key={item.numero} style={[Style.InfoCard, Style.InfoCard.horizontal, { marginBottom:0}]}
          content={
            <CardContent>
              <Center>
              <Horizontal>
                <Icon icon='Pendiente' size={50} style={{tintColor: Theme.color.iconos}}></Icon>
                <CardContent>
                <InfoTitle>Reporte número {item.numero}. Tarea {item.numtarea}</InfoTitle>
                <SubInfo>{item.fecha}</SubInfo>
                </CardContent>
              </Horizontal>
              </Center>
            </CardContent>
          }>
            <CardContent>
              <InfoTitle>Reporte: <SubInfo>{item.reporte}</SubInfo></InfoTitle>
              <InfoTitle>Área de trabajo: <SubInfo>{item.area}</SubInfo></InfoTitle>
              <InfoTitle>Tarea: <SubInfo>{item.tarea}</SubInfo></InfoTitle>
              <InfoTitle>Trabajador: <SubInfo>{item.empleado}</SubInfo></InfoTitle>
            </CardContent> 
          </AccordionItem>
        );
      })}
      
    </Wrap>
  );
} catch {
  return (
    <ErrorCard style={Style.Device.vista}></ErrorCard>
  );
}
};

export const InfoUserArea = ({list}) => {
  if (isEmpty(list)) {
    return (
      <ErrorCard style={Style.Device.vista}></ErrorCard>
    );
  }
try{
  return (
    <Wrap>
      
      {list.map((item, index) => {
        
        return (
              <CardContent key={index} style={[Style.TopCard, {height: "100%"}]}>
                <InfoTitle>Área número: {item.numero}</InfoTitle>
                <InfoTitle>{item.area}</InfoTitle> 
                {/* <SubInfo>{item.}</SubInfo> */}
              </CardContent>
        );
      })}
      
    </Wrap>
  );
} catch{
  return (
    <ErrorCard  style={Style.Device.vista}></ErrorCard>
  );
}
};


export const TareasR = ({list}) => {
  if (isEmpty(list)) {
    return (
      <ErrorCard style={Style.Device.vista}></ErrorCard>
    );
  }
try{
  return (
    <Wrap>
             {list.map((item, index) => {

      return (
        <CardContent key={index} style={[Style.TopCard, {height: "50"}]}>
        <InfoTitle>Tarea número {item.numero}</InfoTitle>
        <SubInfo>{item.descripcion}</SubInfo>
        <InfoTitle>{item.area}</InfoTitle> 
      </CardContent>

      );
        })}
    </Wrap>
    );
    } catch{
            return (
    <ErrorCard  style={Style.Device.vista}></ErrorCard>
  );
}
};

export const TareasRR = ({list}) => {
  const Navigate = Screens();

  if (isEmpty(list)) {
    return (
      <ErrorCard style={Style.Device.vista}></ErrorCard>
    );
  }

try{
  return (
    <Wrap>
      
      {list.map((item, index) => {
        
        return (
            <CardTouch onPress={() => Navigate.Reportar(item)}  key={item.numero} style={[Style.InfoCard, Style.InfoCard.horizontal, ]}>
               <CardContent>
              <Center>
              <Horizontal>
                <Icon icon='Pendiente' size={50} style={{tintColor: Theme.color.iconos}}></Icon>
                <CardContent>
                <InfoTitle>Tarea número {item.numero}</InfoTitle>
                <SubInfo>{item.descripcion}</SubInfo>
                </CardContent>
              </Horizontal>
              </Center>
            </CardContent>
            </CardTouch>
        );
      })}
      
    </Wrap>
  );
} catch{
  return (
    <ErrorCard  style={Style.Device.vista}></ErrorCard>
  );
}
};