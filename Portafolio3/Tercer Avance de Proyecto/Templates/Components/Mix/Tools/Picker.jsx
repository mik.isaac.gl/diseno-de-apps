import React, { useState } from 'react';
import { View, Picker as RNPicker } from 'react-native';

const Picker = ({ tareas, onSelectTarea }) => {
  const [selectedTarea, setSelectedTarea] = useState('');

  const handleTareaChange = (itemValue) => {
    setSelectedTarea(itemValue);
    onSelectTarea(itemValue);
  };

  return (
    <View style={{ borderWidth: 1, borderColor: 'gray', borderRadius: 5 }}>
      <RNPicker
        selectedValue={selectedTarea}
        onValueChange={handleTareaChange}
      >
        <RNPicker.Item label="Selecciona una tarea..." value="" />
        {tareas.map((tarea, index) => (
          <RNPicker.Item key={index} label={tarea.nombre} value={tarea.numero} />
        ))}
      </RNPicker>
    </View>
  );
};

export default Picker;
