import { View } from "react-native"
import { H2, CardTouch,Icon } from "../../Root/Main"
import { buttonCard, center } from "../../../Styles/Style"
import * as Theme from "../../../Styles/Theme"

export const BtnIconText = ({icon, children, style, onPress}) => {
    return(
      <View>
        <CardTouch onPress={onPress} style={[buttonCard, style]}>
          <View style={center}>
            <Icon icon={icon} size={50} style={{tintColor: Theme.color.iconos}}/>
          </View>
        </CardTouch>
        <H2 style={{alignSelf: 'center', }}>{children}</H2>
      </View>
    )
}