import React from 'react';
import {Vertical} from '../../Root/Main';
import { BtnIconText } from './BtnIconText';
import * as theme from '../../../Styles/Theme';


const Botonera = () => {
  
  return (
    <>
      <Vertical style={localStyles}>
        <BtnIconText icon="Employees">Empleados</BtnIconText>
        <BtnIconText icon="Factory">Produccion</BtnIconText>
        <BtnIconText icon="Task">Tareas</BtnIconText>
      </Vertical>

      <Vertical style={localStyles}>
        <BtnIconText icon="Process">Procesos</BtnIconText>
        <BtnIconText icon="Storage">Almacen</BtnIconText>
        <BtnIconText icon="dot3">Mas</BtnIconText> 
      </Vertical>
    </>
  );
};

const localStyles = {
  marginBottom: 20,
  marginHorizontal: theme.spacing.s,
  justifyContent: 'space-between',
}


export default Botonera;
