import { Card, Avatar, Alias, H1, Button, Icon, Horizontal } from '../../Root/Main';
import * as Style from "../../../Styles/Style.js";
import * as Theme from "../../../Styles/Theme";
import { View } from 'react-native';
import { Screens } from "../../../../Core/Mains/NavigatorMain";


const UserCard = ({item, onPress}) => {
    const Navigate = Screens();
    const detailData = item && item.length > 0 ? item[0] : null;
    return(
        <Card style={ Style.TopCard }>
            <Horizontal style={{alignSelf: "flex-end", marginTop: 15}} >
                {/* <Icon icon="Notifications" size={25} style={{tintColor: Theme.color.iconos, marginRight: 10}} /> */}
                {/* <Icon icon="Menu" onPress={onPress} size={25} style={{tintColor: Theme.color.iconos}} /> */}
            </Horizontal>

            
            <Horizontal>
                <Avatar url={"https://static.thenounproject.com/png/11404-200.png"} />
                <View style={{marginLeft: 10}}>
                    <H1>{detailData ? detailData.nombre: ""}</H1>
                    <Alias>{detailData ? detailData.puesto : ""}</Alias>
                </View>
            </Horizontal>
          

            <Button onPress={Navigate.Perfil} style={{alignSelf: 'flex-end', marginTop: 30,}} >
                <Horizontal>
                    <Icon icon="Avatar" size={20} style={{tintColor: Theme.color.iconos, marginRight: 5}} /> 
                    <H1>ver perfil</H1>
                </Horizontal>
            </Button>
        </Card>
    )
}

export default UserCard;