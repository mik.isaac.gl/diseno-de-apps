import React, { useState } from 'react';
import { View, Text, ScrollView } from 'react-native';
import * as Style from '../../Styles/Style';
import { Card, CardTouch } from './Card';
import { color } from '../../Styles/Theme';



export const AccordionItem = ({ title, children, content, style }) => {
  const [expanded, setExpanded] = useState(false);
  const [cardStyle, setCardStyle] = useState(Style.AcordionCard); 

  const toggleAccordion = () => {
    setExpanded(!expanded);
    setCardStyle(expanded ? Style.AcordionCard : [Style.AcordionCard, 
        {borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        borderBottomWidth: 0,
        marginBottom: 0,}]);
  };

  return (
    <View>
        <CardTouch onPress={toggleAccordion} style={[cardStyle, style]}>
          {content}
        </CardTouch>
        {expanded &&
            <View style={Style.AcordionCard.SubCard}>
                {children}
            </View>
        }
    </View>
  );
};