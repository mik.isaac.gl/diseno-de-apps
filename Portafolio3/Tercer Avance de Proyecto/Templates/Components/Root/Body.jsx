import { viewFlex } from "../../Styles/Style.js";
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Animated , {SlideInLeft, SlideOutLeft } from 'react-native-reanimated';



const Body = ({children, style, animationIn=SlideInLeft, animationOut=SlideOutLeft}) => {
    const insets = useSafeAreaInsets();
    return(
        <Animated.View entering={animationIn} exiting={animationOut} style={[viewFlex, {paddingTop: insets.top + 10}, style]}>
            {children}            
        </Animated.View>
    )
};

export default Body; 