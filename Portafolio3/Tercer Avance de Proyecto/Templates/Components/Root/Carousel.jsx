import React from 'react';
import {FlatList} from 'react-native';
import { spacing} from '../../Styles/Theme';
import * as Style from '../../Styles/Style';

const Carousel = ({renderItem, items = [], style, styleItem, children}) => {
  return (
    <FlatList
      data={items}
      horizontal
      style={[Style.CarrouselCard, style]}
      snapToInterval={Style.CarrouselCard.static.withAndSpace-40}
      decelerationRate="fast"
      showsHorizontalScrollIndicator={false}
      keyExtractor={i => i.numero}
      renderItem={({item, index}) => {
        if (renderItem) {
          return renderItem({
            item,
            index,
            style: [{marginRight: index === items.length - 1 ? 0 : spacing.l,}, styleItem], 
          });
        }
        return null;
      }}
    />
  );
};

export default Carousel;
