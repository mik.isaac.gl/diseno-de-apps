import { View } from "react-native"
import { wraper } from "../../Styles/Style"

export const Wrap = ({children}) => {
    return (
        <View style={wraper}>{children}</View>
    )
}