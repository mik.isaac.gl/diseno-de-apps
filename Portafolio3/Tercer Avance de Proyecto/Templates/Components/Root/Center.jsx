import { View } from "react-native"
import { center } from "../../Styles/Style"

export const Center = ({children}) => {
    return (
        <View style={[center]}>
            {children}
        </View>
    )
}