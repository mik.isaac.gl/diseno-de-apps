import { Image, View, StyleSheet } from "react-native"
import * as Style from "../../Styles/Style";
import { sizes } from "../../Styles/Theme";

export const Avatar = ({url, style}) => {
    return(
        <View style={Style.avatar}>
            <Image style={Style.image} source={{uri: url}}/>
        </View>
    )
}

export const BigAvatar = ({source, url, style}) => {
  const img = url || source;
  return(
      <View style={Style.bigAvatar}>
          <Image style={Style.image} source={{uri: img}}/>
      </View>
  )
}

export const Media = ({source, borderBottomRadius = true, style}) => {
    return (
      <View
        style={[Style.media, style].concat(
          borderBottomRadius ? Style.media.borderBottomRadius : null,
        )}>
        <Image style={Style.image} source={{uri: source}} />
      </View>
    );
  };
