import {Dimensions} from 'react-native';
// import * as Font from 'expo-font';
export const {width, height} = Dimensions.get('window');

const colores = {
  rojo: '#FF0000',
  verde: '#00FF00',
  azul: '#0000FF',
  amarillo: '#FFFF00',
  naranja: '#FF8200',
  morado: '#800080',
  rosa: '#FFC0CB',
  blanco: '#FFFFFF',
  negro: '#000000',
  negroLight: '#1D1D1D',
  gris: '#808080',
  grisClaro: '#E5E3E3',
  grisFondo: '#EAEBF0'
};

const statics = {
  text: colores.blanco,
  primary: "#6D4B70",
  secundary: "#2D3154",
  tertiary: "#4B7583",
  fourth: colores.blanco,
  transparent: "transparent",

};

const Tema2 = {
  //  "#EAEBF0"
  status: "dark",
  fondo: colores.blanco,
  subFondo: colores.grisFondo,
  texto: {
    normal: colores.negro,
    inverso: colores.blanco,
    estatico: statics.text,
    estaticoInverso: colores.negro,
    light: colores.grisClaro,
  },
  iconos: colores.blanco,
  bordes: {
    color1: statics.tertiary, 
    ancho1:5,
    color2: colores.blanco, 
    ancho2: 2,
  },
  primary: statics.secundary,
  secundary: statics.secundary,
  tertiary: statics.tertiary,
  fourth: statics.fourth,
  fiveth: colores.naranja,
  nav: {
    fondo: statics.secundary,
    icon: colores.gris,
    iconFocus: colores.blanco,
    indicador: colores.transparent,
  },
  
};

const Tema2dark = {
  //  "#EAEBF0"
  status: "light",
  fondo: colores.negro,
  subFondo: colores.negro,
  texto: {
    normal: colores.blanco,
    inverso: colores.negro,
    estatico: statics.text,
    estaticoInverso: colores.negro,
    light: colores.grisClaro,
  },
  iconos: colores.blanco,
  bordes: {
    color1: colores.blanco, 
    ancho1: 4,
    color2: colores.blanco, 
    ancho2: 3,
  },
  primary: statics.secundary,
  secundary: statics.secundary,
  tertiary: statics.tertiary,
  fourth: statics.fourth,
  fiveth: colores.naranja,
  nav: {
    fondo: statics.secundary,
    icon: colores.gris,
    iconFocus: colores.blanco,
    indicador: colores.transparent,
  },
  
};

const light = {
  status: "dark",
  fondo: colores.blanco,
  subFondo: colores.grisFondo,
  texto: {
    normal: colores.negro,
    inverso: colores.blanco,
    estatico: statics.text,
    light: colores.grisClaro,
    estaticoInverso: colores.negro,
  },
  iconos: colores.blanco,
  bordes: {
    color1: colores.negro, 
    ancho1:2,
    color2: colores.blanco, 
    ancho2:6,
  },
  primary: statics.primary,
  secundary: statics.secundary,
  tertiary: statics.tertiary,
  fourth: statics.fourth,
  nav: {
    fondo: statics.primary,
    icon: colores.gris,
    iconFocus: colores.blanco,
    indicador: colores.transparent,
  },
};

const dark = {
  status: "light",
  fondo: colores.negro,
  subFondo: colores.negro,
  texto: {
    normal: colores.blanco,
    inverso: colores.negro,
    estatico: statics.text,
    estaticoInverso: colores.negro,
    light: colores.grisClaro,
  },
  iconos: colores.blanco,
  bordes: {
    color1: colores.blanco, 
    ancho1:2,
    color2: colores.blanco, 
    ancho1:6,
  },
  primary: statics.primary,
  secundary: statics.secundary,
  tertiary: statics.tertiary,
  fourth: statics.fourth,
  nav: {
    fondo: statics.primary,
    icon: colores.gris,
    iconFocus: colores.blanco,
    indicador: colores.transparent,
  },
};

export const sizes = {
  width,
  height,
  title: 50,
  h2: 24,
  h3: 18,
  body: 14,
  caption: 12,
  radius: 16,
  full: '100%'
};

export const spacing = {
  s: 8,
  m: 18,
  l: 24,
  xl: 40,
};

export const color = Tema2;

  // status: "light",
  // fondo: "#000000",
  // card: "#4F4859",
  // barra: "#6D4B70",
  // topGrafica: "#47516C",
  // desplegable: "#1D1D1D",
  // text: "#FFFFFF",


  // const loadFonts = async () => {
  //   await Font.loadAsync({
  //     'Abel-Regular': require('../../assets/Fuentes/Abel/Abel-Regular.ttf'),
  //   })
  // }

  // loadFonts()