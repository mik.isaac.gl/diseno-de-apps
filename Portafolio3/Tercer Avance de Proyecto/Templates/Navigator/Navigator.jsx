import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from 'expo-status-bar';
import { color } from '../Styles/Theme';

import  {
  Login,
  ForgotPassword,
  Codigo,
  Password,
  Loading,
  Perfil,
  Employees,
  EmployeesArea,
  Produccion,
  Tareas,
  TareasArea,
  Asignar,
  Procesos,
  Almacen,
  Modelos,
  DetailModelos,
  Piezas,
  DetailPieza,
  Materiales,
  DetailMaterial,
  Reportar,

}  from "../../Core/Mains/ScreensMain"
import {default as Tab} from "./TabNavigator";
import { default as tab } from "./tab/Tab";

const Stack = createNativeStackNavigator();

function Navigator() {
  return (
    <NavigationContainer >
      <StatusBar style={color.status} />
        <Stack.Navigator initialRouteName='Loading'>
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false,unmountOnBlur: true }}/>
            <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{ headerShown: false,unmountOnBlur: true }}/>
            <Stack.Screen name="Codigo" component={Codigo} options={{ headerShown: false,unmountOnBlur: true }}/>
            <Stack.Screen name="Password" component={Password} options={{ headerShown: false,unmountOnBlur: true }}/>
            <Stack.Screen name="Tab" component={tab} options={{ headerShown: false,unmountOnBlur: true }} />
            <Stack.Screen name="Loading" component={Loading} options={{ headerShown: false,unmountOnBlur: true }} />
            <Stack.Screen name="Perfil" component={Perfil} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Empleados" component={Employees} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="EmpleadosArea" component={EmployeesArea} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Produccion" component={Produccion} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Tareas" component={Tareas} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="TareasArea" component={TareasArea} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Asignar" component={Asignar} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Reportar" component={Reportar} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Procesos" component={Procesos} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Almacen" component={Almacen} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Modelos" component={Modelos} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="DetailModelos" component={DetailModelos} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Piezas" component={Piezas} options={{ headerShown: false , unmountOnBlur: true}} />
            <Stack.Screen name="DetailPieza" component={DetailPieza} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="Materiales" component={Materiales} options={{ headerShown: false, unmountOnBlur: true }} />
            <Stack.Screen name="DetailMaterial" component={DetailMaterial} options={{ headerShown: false, unmountOnBlur: true }} />
        </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigator;