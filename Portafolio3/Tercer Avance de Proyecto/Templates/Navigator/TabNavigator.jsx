import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {Devices, Home, Tools, Extras}  from "../../Core/Mains/ScreensMain"
import { Icon } from '../Components/Mix/Main';
import * as TavNav from '../Styles/Style';
// import CustomTabBar from './tab/CustomTabBar';
import CustomTabBarButton from './tab/CustomTabBarButtom';
const TabBar = () => {
  const Tab = createMaterialTopTabNavigator();

  return (
    <Tab.Navigator 
      initialRouteName="Home" 
      tabBarPosition="bottom"
      screenOptions={TavNav.screenOptions}
      >
      <Tab.Screen name="Devices" component={Devices}
        options={{
          tabBarIcon: ({focused}) => (
            <Icon icon="Devices" size={TavNav.Icon.size} style={{tintColor: focused? TavNav.Icon.focusTrue: TavNav.Icon.focusFalse}} />
          ),
          tabBarButton: props => <CustomTabBarButton route="Devices" {...props} />
        }} />
      <Tab.Screen name="Home" component={Home} 
        options={{
          tabBarIcon: ({focused}) => (
            <Icon icon="Home" size={TavNav.Icon.size} style={{tintColor: focused? TavNav.Icon.focusTrue: TavNav.Icon.focusFalse}}/>
          ),
        }} />
      <Tab.Screen name="Tools" component={Tools} 
        options={{
            tabBarIcon: ({focused}) => (
              <Icon icon="Settings" size={TavNav.Icon.size} style={{tintColor: focused? TavNav.Icon.focusTrue: TavNav.Icon.focusFalse}}/>
            ),
          }} />
    </Tab.Navigator>
  );

  // const tabs = [
  //   {
  //     name: 'Home',
  //     activeIcon: <Icon name="Home" color="#fff" size={25} />,
  //     inactiveIcon: <Icon name="Home" color="#4d4d4d" size={25} />
  //   },
  //   {
  //     name: 'Devices',
  //     activeIcon: <Icon name="Devices" color="#fff" size={25} />, // Asegúrate de usar el nombre correcto del icono
  //     inactiveIcon: <Icon name="Devices" color="#4d4d4d" size={25} />
  //   },
  //   {
  //     name: 'Tools',
  //     activeIcon: <Icon name="Settings" color="#fff" size={25} />, // Asegúrate de usar el nombre correcto del icono
  //     inactiveIcon: <Icon name="Settings" color="#4d4d4d" size={25} />
  //   },
  // ];

  // return (
  //   <Tabbar
  //     tabs={tabs}
  //     tabBarContainerBackground='#6699ff'
  //     tabBarBackground='#fff'
  //     activeTabBackground='#6699ff'
  //     labelStyle={{ color: '#4d4d4d', fontWeight: '600', fontSize: 11 }}
  //     onTabChange={() => console.log('Tab changed')}
  //   />
  // );
};

export default TabBar;



// import Tabbar from "@mindinventory/react-native-tab-bar-interaction";


