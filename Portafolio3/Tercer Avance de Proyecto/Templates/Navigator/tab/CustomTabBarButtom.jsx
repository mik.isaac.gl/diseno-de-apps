import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import * as Theme from '../../Styles/Theme';
import Svg, {Path} from 'react-native-svg';
import { Icon } from '../../Components/Root/Icon';


const CustomTabBarButton = props => {
  const {route, children, accessibilityState, onPress} = props;

  if (accessibilityState.selected) {
    return (
      <View style={styles.btnWrapper}>
        <View style={{flexDirection: 'row'}}>
          <View
            style={[
              styles.svgGapFiller,
              {
                borderTopLeftRadius: route === 'Devices' ? 10 : 0,
                borderBottomLeftRadius: route === 'Devices' ? 10 : 0,
              },
            ]}
          />
          <Svg width={78} height="100%" viewBox="0 0 0 0">
            <Path
              d="M75.2 0v61H0V0c4.1 0 7.4 3.1 7.9 7.1C10 21.7 22.5 33 37.7 33c15.2 0 27.7-11.3 29.7-25.9.5-4 3.9-7.1 7.9-7.1h-.1z"
              fill={Theme.color.tertiary}
            />
          </Svg>
          <View
            style={[
              styles.svgGapFiller,
              {
                borderTopRightRadius: route === 'Settings' ? 10 : 0,
                borderBottomRightRadius: route === 'Settings' ? 10 : 0,
              },
            ]}
          />
        </View>

        <TouchableOpacity
          activeOpacity={1}
          onPress={onPress}
          style={[styles.activeBtn]}>
          <Icon icon={route} size={30} style={{tintColor: Theme.color.nav.iconFocus}}/>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={onPress}
        style={[
          styles.inactiveBtn,
          {
            borderTopLeftRadius: route === 'Devices' ? 10 : 0,
            borderTopRightRadius: route === 'Settings' ? 10 : 0,
            borderBottomLeftRadius: route === 'Devices' ? 10 : 0,
            borderBottomRightRadius: route === 'Settings' ? 10 : 0,
          },
        ]}>
        <Icon icon={route} size={30} style={{tintColor: Theme.color.nav.icon.h}}/>
      </TouchableOpacity>
    );
  }
};

export default CustomTabBarButton;

const styles = StyleSheet.create({
  btnWrapper: {
    flex: 1,
    alignItems: 'center',
  },
  activeBtn: {
    flex: 1,
    position: 'absolute',
    top: -22,
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: Theme.color.tertiary,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5,
  },
  inactiveBtn: {
    flex: 1,
    backgroundColor: Theme.color.tertiary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  svgGapFiller: {
    flex: 1,
    backgroundColor: Theme.color.tertiary,
  },
});