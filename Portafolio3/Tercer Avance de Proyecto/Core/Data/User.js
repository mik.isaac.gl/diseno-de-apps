import AsyncStorage from '@react-native-async-storage/async-storage';

export async function getUser() {
    try {
        const user = await AsyncStorage.getItem('User');
        return user;
    } catch (error) {
        console.error('Error al recuperar el usuario:', error.message);
        return null;
    }
  }

//Metodo de creacion del usuario
export async function setUser(user) {
    try {
        await AsyncStorage.setItem('User', String(user));
        console.log('Usuario creado'+ getUser());
    } catch (error) {
        console.error('Error al crear al usuario:', error.message);
    }
    
}

//Metodo de Eliminacion del usuario
export async function removeUser() {
    try {
        await AsyncStorage.removeItem('User');
    } catch (error) {
        console.error('Error al eliminar el usuario:', error.message);
    }    
}


