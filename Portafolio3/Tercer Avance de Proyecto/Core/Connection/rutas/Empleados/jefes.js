import  * as Token  from "../../../Data/Token";
import { useEffect, useState } from "react";
import { URL } from "../../Global";

const extension = 'jefesArea';
const endpoint = `${URL}${extension}`;

export const jefes = () => {
    const [jefes, setJefes] = useState([]);

    useEffect(() => {
        const fetchJefes = async() => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const data = await response.json();
                setJefes(data);
            } catch {
                setJefes([]);              
            }
        };

        fetchJefes();
    }, []);

    return jefes;
};