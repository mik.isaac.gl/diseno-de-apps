import  * as Token  from "../../../Data/Token";
import { useEffect, useState } from "react";
import { URL } from "../../Global";

const extension = 'empleados';
const endpoint = `${URL}${extension}`;

export const empleados = () => {
    const [empleados, setEmpleados] = useState([]);

    useEffect(() => {
        const fetchEmpleados = async() => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const data = await response.json();
                setEmpleados(data);
            } catch {
                setEmpleados([]);              
            }
        };

        fetchEmpleados();
    }, []);

    return empleados;
};