import  * as Token  from "../../../Data/Token";
import { useEffect, useState } from "react";
import { URL } from "../../Global";

const extension = 'tareas';
const endpoint = `${URL}${extension}`;

export const tareas = () => {
    const [tareas, setTareas] = useState([]);

    useEffect(() => {
        const fetchEmpleados = async() => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const data = await response.json();
                setTareas(data);
            } catch {
                setTareas([]);              
            }
        };

        fetchEmpleados();
    }, []);

    return tareas;
};