import  * as Token  from "../Data/Token";
import * as User from "../Data/User";
import { URL } from "./Global";

export async function Login(correo, contrasena) {
  try {
    //Conexion del login con el servidor
    const response = await fetch(URL+'login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        correo,
        contrasena,
      }),
    });

    //Si la respuesta no es ok  genera un error
    if (!response.ok) {
      const errorResponse = await response.json();
      throw new Error(errorResponse.error);
    }

    //Si la respuesta es ok, guarda el token
    const data = await response.json();
    console.log(data);
    Token.setToken(data.token);
    User.setUser(data.numero);
    return data;
    
  } catch (error) {
    console.error('Error al iniciar sesión:', error.message);
    throw error;
  }
}

export async function solicitarCodigo(correo) {
  try {
    //Conexion del login con el servidor
    const response = await fetch(URL+'solicitar', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        correo,
      }),
    });
    
  } catch (error) {
    console.error('Error al enviar el código:', error.message);
    throw error;
  }
}

export async function IngresarCodigo(codigo) {
  try {
    //Conexion del login con el servidor
    const response = await fetch(URL+'codigo', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        codigo,
      }),
    });

    //Si la respuesta no es ok  genera un error
    if (!response.ok) {
      const errorResponse = await response.json();
      throw new Error(errorResponse.error);
    }

    //Si la respuesta es ok, guarda el token
    const data = await response.json();
    Token.setToken(data.token);
    return data;
    
  } catch (error) {
    console.error('Error al iniciar sesión:', error.message);
    throw error;
  }
}

export async function Password(contrasena) {
  try {
    //Conexion del login con el servidor
    const authToken = await Token.getToken();
    const response = await fetch(URL+'contrasena', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${authToken}`
      },
      body: JSON.stringify({
        contrasena,
      }),
    });

    if (response.ok) {
      console.log('Contraseña actualizada con éxito');
    } else {
      const errorMessage = await response.text();
      throw new Error(`Error al actualizar la contraseña: ${errorMessage}`);
    }

  } catch (error) {
    console.error('Error al iniciar sesión:', error.message);
    throw error;
  }
}
