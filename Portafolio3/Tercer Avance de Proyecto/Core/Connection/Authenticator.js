import  * as Token  from "../Data/Token";
import { URL } from "./Global";

//Valida el token existente en el servidor
export const AuthToken = async () => {
    try {
        //Extraer el token guardado
        const authToken = await Token.getToken();

        //Principal conexion con la api de authentification
        const responsePromise = fetch(URL + 'auth', {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${authToken}`
            }
        });

        //Contador de 10 segundos para conectarse al servidor
        const timeoutPromise = new Promise((_, reject) =>
            setTimeout(() => reject(new Error('Timeout')), 10000)
        );

        //Respuesta de la conexion de la api y el contador
        const response = await Promise.race([responsePromise, timeoutPromise]);

        if (response.ok) {
            //Token autentificado con el servidor
            return true;
        } else {
            //Respuesta negativa del server desautentificando el token
            console.log('Error en la autenticación:', response);
            return false;
        }
    } catch (error) {
        if (error.message === 'Timeout') {
            //Errores de conexion al tardar en conectarse al servidor
            console.log('Tiempo de espera excedido. Por favor, verifica tu conexión a internet.');
        } else {
            console.log('Error al verificar el token:', error);
        }
        return false;
    }
};

//Valida la aexistencia de un token en el dispositivo
export const AuthState = async () => {
    try {
        const token = await Token.getToken();
        if (token !== null) {
            // Llama a AuthToken y espera su resultado
             const authenticated = await AuthToken();
            return authenticated;
        } else {
            
            //No existe un token en el dispositivo
            console.log('El usuario no está autenticado');
            return false;
        }
    } catch  {
        console.log('Error al verificar el estado de autenticación');
        return false;
    }
};
