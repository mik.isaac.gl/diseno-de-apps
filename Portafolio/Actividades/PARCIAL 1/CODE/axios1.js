const axios = require('axios'); // Requiere la librería Axios
const fetch = require('node-fetch'); // Requiere la función fetch en Node.js

const url = "https://jsonplaceholder.typicode.com/users"; // URL para las solicitudes

// Utiliza Axios para realizar una solicitud GET y mostrar los nombres de usuario de los usuarios devueltos
axios.get(url).then(response => {
    response.data.forEach(element => {
        console.log(element.username);
    });
});

// Utiliza la función fetch para realizar una solicitud GET y mostrar los nombres de usuario de los usuarios devueltos
fetch(url)
    .then(response => response.json())
    .then(response => {
        response.forEach(element => {
            console.log(element.username);
        });
    });

// Utiliza Axios para realizar una solicitud POST con datos de usuario y muestra los datos de la respuesta
axios.post(url, {
    username: "Foo Bar",
    email: "foo@bar.com"
}).then(response => console.log(response.data));
