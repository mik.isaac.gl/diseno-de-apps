// Imprime "Hello world xd" en la consola
console.log("Hello world xd");

// Declaración de variables
var s = "Foo Bar";
let x = 90;
var y = 89;

// Declaración de un array
var array = [1, 2, 3, 4, 5, "Foo", "Bar", true, false, 2.34, 4.23];

// Declaración de un objeto
var obj = { 
    first_name: "Foo",
    last_name: "Bar", 
    age: 23, 
    city: "TJ", 
    status: true ,  
    arr: array 
};

// Bucle for que imprime números del 0 al 99
for (let i = 0; i < 100; i++) {
    console.log(i);
}

// Bucle for que imprime cada elemento del array
for (let i = 0; i < array.length; i++) {
    console.log(array[i]);
}

// Bucle for...of incorrecto, debe ser for...of array, no array.length
for (let i of array.length) {
    console.log(array[i]);
}

// Bucle for...of que itera a través de las claves del objeto
for(let key of Object.keys(obj)) {
    console.log(key + ": " + obj[key]);
}

// Bucle for...in que itera a través de las propiedades del objeto
for(let key in obj) {
    console.log(key + ": " + obj[key]);
}

// Bucle while que multiplica i por 5 hasta que sea mayor o igual a 1000
var i = 1;
while(i < 1000){
    i *= 5;
    console.log(i);
}

// Bucle do...while que aumenta o disminuye i hasta llegar a 100000000 o 1
var l = true;
do {
    console.log(i);
    if (i == 100000000) {
        l = false;
    } else if (i == 1) {
        l = true;
    }
    if (l) {
        i *= 10;
    } else {
        i /= 10;
    }
} while(true);

// Función que espera una cantidad específica de milisegundos
function esperar(milliseconds) {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
}

// Función asíncrona que espera 2 segundos antes de imprimir un mensaje
async function miFuncionConEspera() {
  console.log("Inicio de la función");
  await esperar(2000);
  console.log("Fin de la función después de esperar 2 segundos");
}

// Llamada a la función asíncrona
miFuncionConEspera();

// Reasignación de la variable s a true
s = true;

// Impresión de distintas variables y expresiones
console.log(s);
console.log(x + y);
console.log(s);
console.log(array);
console.log(array[5]);
console.log(obj);
console.log(obj["first_name"]);
console.log(obj.arr);

// Estructura condicional if...else
if (x > y) {
    console.log("si");
} else {
    console.log("no");
}

// Estructura condicional switch
var opc = 1;
switch (opc) {
    case 1:
        console.log("1");
        break;
    case 2:
        console.log("2");
        break;
    case 3:
        console.log("3");
        break;
    default:
        console.log("default");
        break;
}

// Operador ternario
var animal = "Kitty";
var hello = (animal === "Kitty") ? "It is a pretty kitty" : "It is not a pretty kitty";
console.log(hello);

// Función anidada
function foo() {
    var a = "kitty";
    function xd() {
        console.log(a);
    }
    xd();
}

foo();

// Función de cálculo del volumen de un prisma
var prism = function(l, w, h) {
    return l * w * h;
};
console.log("Volumen del prisma:" + prism(23, 56, 12));

// Función que retorna otra función para calcular el volumen de un prisma
function prisma(l) {
    return function(w) {
        return function(h) {
            return l * w * h;
        };
    };
}
console.log("Volumen del prisma: " + prisma(23)(12)(56));

// Función auto-invocada
const foo = (function() {
    console.log("Me gustan los potatos");
}());

// Declaración de una función con nombre y una función anónima
var namedsum = function sum (a, b) {
    return a + b;
};

var anonSum = function(a, b) {
    return a + b;
};

console.log(anonSum(5, 5));

// Función recursiva
var say = function say(times) {
    say = undefined;
    if (times > 0) {
        console.log("hello");
        say(times - 1);
    }
};

var saysay = say;
say = "ops";
saysay(5);
console.log(say);

// Función con un parámetro predeterminado
function foo(msg = "Me gustan las naranjas") {
    console.log(msg);
}
foo();

// Llamada a una API usando fetch para obtener datos de Stack Exchange y JSONPlaceholder
var url = "https://api.stackexchange.com/2.2/questions/featured?order=desc&sort=activity&site=stackoverflow";
var responseData = fetch(url).then(response => response.json());
responseData.then(({items, has_more, quota_max, quota_remaining}) => {
    for (var {title, score, owner, link, anwser_count} of items) {
        console.log('Q:' + title + " owner: " + owner.display_name);
    }
});

var url = "https://jsonplaceholder.typicode.com/users";
var responseData = fetch(url).then(response => response.json())
                             .then(response => {
                                response.forEach(user => {
                                    console.log(user.username);
                                });
                              });

var url = "https://jsonplaceholder.typicode.com/posts";
fetch(url).then(response => response.json())
         .then(response => {
            response.forEach(posts => {
                console.log(posts.title);
            });
          });

